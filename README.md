# Kde.org website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_kde-org)](https://binary-factory.kde.org/job/Website_kde-org/)

This is the git repository for kde.org, the main KDE Community website.

As a (Hu)Go module, it requires both Hugo and Go to work.

## Development
Read about the shared theme at [kde-hugo wiki](https://invent.kde.org/websites/aether-sass/-/wikis/Hugo).

Since this repo is pretty big, you might want to clone with this command
```
git clone --depth 5 git@invent.kde.org:websites/kde-org.git
```

This will perform a shallow clone, reducing the amount of data that needs to be downloaded without usually impacting your workflow. Remove the `--depth 5` part if you want to do a full clone.

## I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).

## Licensing
We assume new contributions to the content are licensed under CC-BY-4.0 and to the websites code under LGPL-3.0-or-later unless specified otherwise.
