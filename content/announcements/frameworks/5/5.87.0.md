---
qtversion: 
date: 2021-10-09
layout: framework
libCount: 83
---


### Attica

* Do not mark jobs that have been aborted as errored (bug 43820)
* Ensure categories.xml is only fetched once in parallel
* Do not start basejobs twice

### Breeze Icons

* Fix KTimeTracker icon sizes (bug 442993)
* Tweaks to AnyDesk icons
* Fix index.theme issues from !124
* Add 22px variants of the preferences icons
* Add AnyDesk icons
* Add more Godot MIME icons
* Add symlink for skanlite's new icon name
* Add process-working-symbolic, overhaul 22px animation

### Extra CMake Modules

* Add -Werror=init-self to the default CMAKE_CXX_FLAGS
* Handle git remotes that aren't called origin in _repository_name()
* python: Bump maximum version for Python 3 module generator check
* Avoid raising an error for submodule git trees
* Make sphinx happier about syntax

### KCalendarCore

* Set the correct last modified time when creating an exception for a recurring event
* icalformat_p.cpp - support ATTACH parameter FILENAME (bug 436161)
* Add Qt5Gui as dependency in pkgconfig file
* Read floating date time as LocalTime
* Implement PERIOD support in RDATE (bug 271308)

### KCMUtils

* Fix Warning: Property type "Int" is not a known QVariant type
* Add support for static builds
* Add compat code for KService based plugin loading
* Introduce KCModuleProxy::isChanged method
* Un-overload KCModuleProxy:changed signal

### KCodecs

* KCodecs::decodeRFC2047String(): return "UTF-8" when multiple charsets used

### KCompletion

* New shouldAutoSuggest property on KCompletion (bug 420966)

### KConfig

* Android: Fix writing to config if path is a content:// Uri
* kconfigini: Only open the file once to write
* Allow KConfigXT to use KSharedConfig::openStateConfig

### KConfigWidgets

* Do not emit deprecation warnings for overload which gets chosen by compiler

### KCoreAddons

* KStringHandler: add a new perlSplit() overload that takes a QStringView
* Deprecate KPluginMetaData::extraInformation

### KDBusAddons

* Introduce UpdateLaunchEnvJob
* Correct to make sure we use the matching ECM version

### KDeclarative

* Make sure the object gets destroyed before the view
* Add support for static builds
* SimpleKCM: remove custom header and footer handling

### KGlobalAccel

* Remove obsolete LGPL-2.0-only license text
* Relicense remaining LGPL-2.0-only file

### KDE GUI Addons

* Use imported target for X11 libs
* Relicense remaining files away from LGPL-2.0-only
* Add missing static dependency to Config.cmake.in

### KHolidays #

* Update Canadian holidays

### KImageFormats

* avif: performance and quality improvements

### KInit

* Fix KDE shutdown bug

### KIO

* New job: KEMailClientLauncherJob
* KACLEditWidget: improve the initial widget size
* [KUrlNavigator] Add the ability to show hidden folders in the subdirectories popup
* Add support for static builds
* KUrlCompletion blocks autosuggestion from happening if the input is an exact directory path (bug 420966)
* KDirOperator: provide an option to enable showing open-with item actions (bug 440748)
* Fix permissions when copying files (bug 103331)
* KFileItem: refresh() shouldn't discard ACL attributes (bug 299155)
* KPropertiesDialog: show text label with item name for readonly items too (bug 442237)
* Disable ACL functionality in kpropertiesdialog on FreeBSD
* KDirOperator: use show() when opening KPropertiesDialog
* Use errors=remount-ro when mounting ext2/3/4
* OpenUrlJob: skip HTTP schemeHandler when setEnableExternalBrowser(false)
* KMountPoint: restore findByPath() behaviour, i.e. resolve symlinks
* Sort service menus before inserting them in context menu
* Deprecate config widget related methods in ThumbCreator

### Kirigami

* PageRow: Do not async load the global header (bug 442660)
* NavigationTabButton: Fix hover effect staying even after touch release
* Fix BreadcrumbControl on mobile when using layers
* cmake: Remove intermediate target for kirigamiplugin post-build steps
* Add NavigationToolBar component
* Split off an AboutItem from AboutPage
* Improve warning about QML Units implementations
* [SwipeListItem] Fix view property
* Fix breadcrumb header title on secondary layers in mobile mode
* [FormLayout] Fix in-group spacing in narrow mode
* Fix page header being shown even when it's specified not to
* Set activeFocusOnTab to false for ListSectionHeader
* Support enter/return and up/down keys in Global drawer for navigation
* ColumnView: Simplify Units property access
* Remove unwanted "/" prefix from iconId

### KItemViews

* Relicense files from LGPL-2.0-only to LGPL-2.0-or-later

### KNewStuff

* staticxml: Do not report installed packages for page != 0
* Engine: Do not require waiting for the providers to tell our installed entries
* Fix crash in DownloadWidget
* Add licenses CC0 license info to non-copyrightable files
* Add BSD-2-Clause license info to cmake files
* Add CC0-1.0 license identifier to non-copyrightable files
* Include a user agent on KNS requests
* attica: use compile-time connects
* Add support for static builds - fixes
* Add missing find_package(Qt5Gui)
* Add support for static builds
* FileCopyJob: implement error handling
* Page: Remind/notify users that everything here is 3rd-party content

### KNotification

* Add support for static builds
* Correct the version of Phonon that we use

### KQuickCharts

* Remove stray GL include

## KRunner

* Remove unneeded dependencies from .kde-ci.yml
* Mark README.md as non-copyrightable
* Add license information for runner C++ template
* Mark non-copyrightable files as CC0-1.0
* Add license information to cmake files

### KService

* Deprecate KAutostart class
* Deprecate KService::parentApp
* Deprecate KService::pluginKeyword property

### KTextEditor

* The user is not selecting as soon as we clearSelection
* Fix missing i18n (bug 442071)

### KTextWidgets

* Handle RTL text selection the same way as Qt (bug 397922)

### KWayland

* KWayland depends on libraries/plasma-wayland-protocols

### KWidgetsAddons

* Make lupdate happier
* KCharSelect: Added option to show all blocks found in the data file (in the section menu)
* KFontChooser: the widget shouldn't become wider when toggling show fixed only

### KXMLGUI

* Add missing Q_INIT_RESOURCE(kxmlgui)
* Replace "Libraries" by "Components" to show KAboutComponents info

### ModemManagerQt

* Introduce countryCode Modem3gpp API

### Plasma Framework

* Update plasmoidheading.svg  : fix the typo line 96 "correntColor"
* Containment: Rename panel edit action to "Enter Edit Mode"
* Revert "Change busywidget to a gear" (bug 442525)
* ExpandableListitem: Correct expanded view height calculation (bug 442537)
* Port internal plugin cache away from supporting multiple namespaces
* Remove defunct python and ruby script engines

### Prison

* Select the most efficient QR encoding mode rather than always using 8bit

### Purpose

* plugins/barcode: Call it a "QR code" in the UI
* Add Barcode plugin

### QQC2StyleBridge

* Properly get the path from a QUrl
* MenuSeparator: fix height being wrong, rewrite whole file to be more correct
* Button, ToolButton: improve implicit sizing, correctly set default button state
* Use more process-working-symbolic icon for busy spinner

### Solid

* Add support for static builds
* Support extracting cpu model on ppc64

### Syntax Highlighting

* Add basic QML API docs
* yara - add new 4.x keywords
* Change license to MIT
* Removed items that are generating errors for now
* initial work on terraform syntax highlight
* Port AbstractHighlighter::highlightLine to QStringView

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
