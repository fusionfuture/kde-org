---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE Ships Applications 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE Ships KDE Applications 18.12.3
version: 18.12.3
---

{{% i18n_date %}}

{{% i18n_var "Today KDE released the third stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../18.12.0" "18.12" %}}

More than twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello, among others.

Improvements include:

- Loading of .tar.zstd archives in Ark has been fixed
- Dolphin no longer crashes when stopping a Plasma activity
- Switching to a different partition can no longer crash Filelight
