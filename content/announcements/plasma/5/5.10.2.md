---
aliases:
- ../../plasma-5.10.2
changelog: 5.10.1-5.10.2
date: 2017-06-13
layout: plasma
youtube: VtdTC2Mh070
figure:
  src: /announcements/plasma/5/5.10.0/plasma-5.10.png
  class: text-center mt-4
asBugfix: true
---

- Discover: Make the PackageKit backend more resistant to crashes in PackageKit. <a href="https://commits.kde.org/discover/64c8778d4cd5180dfa13dd75fed808de9271cedc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/376538">#376538</a>
- Plasma Networkmanager Openconnect: make sure the UI fits into the password dialog. <a href="https://commits.kde.org/plasma-nm/285c7ae37a3f6149b866dfb887bcb62ca6ce1046">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380399">#380399</a>
- Include a header for the settings page for better consistency. <a href="https://commits.kde.org/discover/bffd42fbaac59f262f6d61b8a25cbea5abb12701">Commit.</a>
