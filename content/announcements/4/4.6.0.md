---
aliases:
- ../4.6
date: '2011-01-26'
description: KDE Puts You In Control with New Workspaces, Applications and Platform
custom_about: true
custom_contact: true
title: KDE Puts You In Control with New Workspaces, Applications and Platform
---

</p>
<p>
KDE is delighted to announce its latest set of releases, providing major updates to the KDE Plasma workspaces, KDE Applications and KDE Platform. These releases, versioned 4.6, provide many new features in each of KDE's three product lines. Some highlights include:
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w09.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w09.png" class="img-fluid" alt="KDE Plasma Desktop, Gwenview and KRunner in 4.6">
	</a> <br/>
	<em>KDE Plasma Desktop, Gwenview and KRunner in 4.6</em>
</div>
<br/>

<h3>
<a href="./plasma">
Plasma Workspaces Put You in Control
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.6.0" />
</a>

The<b> KDE Plasma Workspaces</b> gain from a new Activities system, making it easier to associate applications with particular activities such as work or home tasks. Revised power management exposes new features but has a simpler configuration interface. KWin, the Plasma workspace window manager, receives a new scripting and the workspaces receive visual enhancements. <b>Plasma Netbook</b>, optimized for mobile computing devices receives speed enhancements and becomes easier to use via a touchscreen interface. For more details read the <a href="./plasma">KDE Plasma Workspaces 4.6 announcement</a>.

</p>

<h3>
<a href="./applications">
KDE’s Dolphin Adds Faceted Browsing
</a>
</h3>

<p>

<a href="./applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.6.0"/>
</a>
</a>
Many of the <b>KDE Application</b> teams have also released new versions. Particular highlights include improved routing capabilities in KDE’s virtual globe, Marble, and advanced filtering and searching using file metadata in the KDE file manager, Dolphin: Faceted Browsing. The KDE Games collection receives many enhancements and the image viewer Gwenview and screenshot program KSnapshot gain the ability to instantly share images to a number of popular social networking sites. For more details read the <a href="./applications">KDE Applications 4.6 announcement</a>.<br /><br />
</p>

<h3>
<a href="./platform">
Mobile Target Makes KDE Platform Lose Weight
</a>
</h3>

<p>

<a href="./platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.6.0"/>
</a>

The <b>KDE Platform</b>, on which the Plasma Workspaces and KDE Applications are built also gains new capabilities available to all KDE applications. The introduction of a "mobile build target" allows for easier deployment of applications on mobile devices. The Plasma framework gains support for writing desktop widgets in QML, the declarative Qt language, and provides new Javascript interfaces for interacting with data. Nepomuk, the technology behind metadata and semantic search in KDE applications, now provides a graphical interface to back up and restore data. UPower, UDev and UDisks can be used instead of the deprecated HAL. Bluetooth support is improved. The Oxygen widget and style set is improved and a new Oxygen theme for GTK applications allows them to seamlessly merge into the Plasma workspaces and look just like KDE applications. For more details read the <a href="./platform">KDE Platform 4.6 announcement</a>.

</p>

<h4>
    Spread the Word and See What Happens: Tag as "KDE"
</h4>
<p align="justify">
KDE encourages everybody to <strong>spread the word</strong> on the Social Web.
Submit stories to news sites, use channels like delicious, digg, reddit, twitter,
identi.ca. Upload screenshots to services like Facebook, Flickr,
ipernity and Picasa and post them to appropriate groups. Create screencasts and
upload them to YouTube, Blip.tv, Vimeo and others. Do not forget to tag uploaded
material with the <em>tag <strong>kde</strong></em> so it is easier for everybody to find the
material, and for the KDE team to compile reports of coverage for the KDE SC 4.6
 announcement. <strong>Help us spreading the word, be part of it!</strong></p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/news/technology/kde_software_compilation_4_6_0_released"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_460_released/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>Support KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.6.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br />
<p>&nbsp;</p>
