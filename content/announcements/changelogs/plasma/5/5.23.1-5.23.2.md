---
title: Plasma 5.23.2 complete changelog
version: 5.23.2
hidden: true
plasma: true
type: fulllog
---
{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kcm: Fix removal of Xft.dpi from Xresources. [Commit.](http://commits.kde.org/kscreen/bddecd254313e75040ea903bcecfd7abb0be879b) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ [wayland] fix ignored keyboard RepeatRate. [Commit.](http://commits.kde.org/kwin/1dc40064e3abb614c960dab494d7dd0796a48795) Fixes bug [#443721](https://bugs.kde.org/443721)
+ Kwineffects: Fix destruction of s_fbo with shared GLTexture objects. [Commit.](http://commits.kde.org/kwin/1b1aef83b7ebf388c1d9ce6b8925eb89347c4a00) Fixes bug [#443951](https://bugs.kde.org/443951)
+ WaylandOutput: schedule update on mode change. [Commit.](http://commits.kde.org/kwin/1fb8ebc92cbecaf5ddca538e412eb7484e89eb51) Fixes bug [#444303](https://bugs.kde.org/444303)
+ Platforms/drm: fix direct scanout check. [Commit.](http://commits.kde.org/kwin/1738b44b15ec3702d5f1c2e1ecb646fd17405ad5) 
+ Fix focus loss on decoration destruction. [Commit.](http://commits.kde.org/kwin/d87273d8114c4abc72b518ebd300fd67b33a3625) Fixes bug [#411884](https://bugs.kde.org/411884)
+ Input: fix crash on touch quicktile. [Commit.](http://commits.kde.org/kwin/5d0d55e9725019e7ec4c9252b8ac7faee39f5456) 
+ Platforms/drm: fix init with explicit modifiers. [Commit.](http://commits.kde.org/kwin/33f06ee14b5353fc099d26c0f6868b7ab48f047c) 
+ Platforms/drm: allow overriding eglstream choice. [Commit.](http://commits.kde.org/kwin/a8f519dc51f036545008ecf1b07d57fbab86b140) 
+ Platforms/drm: set draw buffer for gbm. [Commit.](http://commits.kde.org/kwin/44c456cf404290d2169a47c437e850976454e281) 
+ Platforms/drm: use gbm with NVidia driver 495+. [Commit.](http://commits.kde.org/kwin/aa8c9ffc93ef3c51d95abab86304478d9580473a) 
{{< /details >}}

{{< details title="Oxygen" href="https://commits.kde.org/oxygen" >}}
+ Fix paint for standalone buttons with dynamic size. [Commit.](http://commits.kde.org/oxygen/42e7e9dc668b7d572be76fe03fc584a7b3c64f89) 
{{< /details >}}

{{< details title="Plasma Browser Integration" href="https://commits.kde.org/plasma-browser-integration" >}}
+ [History Runner] Skip blob URLs. [Commit.](http://commits.kde.org/plasma-browser-integration/9ce049c9b93304afe83ac45a84f25a623fe71326) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [Folder View] Fix executing file without prompting. [Commit.](http://commits.kde.org/plasma-desktop/085f9b7e520a5b5f921a5b9a353d20be73c3d4ad) Fixes bug [#435560](https://bugs.kde.org/435560)
+ KCM Touchpad: Load config for touchpad on init. [Commit.](http://commits.kde.org/plasma-desktop/085874b85383fe6b2c9eee18704ceaa5bb92a3de) Fixes bug [#443011](https://bugs.kde.org/443011)
+ Remove defunct check for proxyAction in foldermodel. [Commit.](http://commits.kde.org/plasma-desktop/b0831510de0af7c50e0f73da60312261eab36359) Fixes bug [#444128](https://bugs.kde.org/444128)
+ Taskmanager: Show highlight before ToolTipInstance starts loading for grouped tooltips. [Commit.](http://commits.kde.org/plasma-desktop/9e1a0670721093b508f1c6722c8c82beb95078b1) See bug [#433907](https://bugs.kde.org/433907)
+ Fix updating apps model. [Commit.](http://commits.kde.org/plasma-desktop/27e5f4e4959b67289280f299d6278c5301a82fc0) Fixes bug [#444101](https://bugs.kde.org/444101)
+ Fix pinned applet closes. [Commit.](http://commits.kde.org/plasma-desktop/3686fea5d56f743e42b89db88c34feee9619f1a0) Fixes bug [#443589](https://bugs.kde.org/443589)
+ Taskmanager: Prevent useless component creation/destruction for grouped tooltips. [Commit.](http://commits.kde.org/plasma-desktop/2f27fc9f05462076a77254c0df17fa07ee8228a8) See bug [#433907](https://bugs.kde.org/433907). See bug [#444001](https://bugs.kde.org/444001)
+ Applets/taskmanager: Always set mprisSourceName to parentPid. [Commit.](http://commits.kde.org/plasma-desktop/262665acc85ad5b3ca8fa6c93bbd8d00a6a5ca31) 
+ Make Applet config window a tiny bit smaller and reduce minimum size. [Commit.](http://commits.kde.org/plasma-desktop/753f82d7fa60ab53bdf17ba8a2a178f04acabc19) Fixes bug [#443712](https://bugs.kde.org/443712)
+ Set Qt.ImhNoPredictiveText for text fields that act as search fields. [Commit.](http://commits.kde.org/plasma-desktop/059a9dfe688bcd9763e793bc1238f3d128df44c6) Fixes bug [#439914](https://bugs.kde.org/439914)
+ Desktop as folder: restore functionality of the "delete" action. [Commit.](http://commits.kde.org/plasma-desktop/9f53bf160b347f76f2d4914408ac9bad6975275b) Fixes bug [#442765](https://bugs.kde.org/442765)
+ Fix create-folder-shortcut (F10). [Commit.](http://commits.kde.org/plasma-desktop/d7bcf6dee0c764cf2fdbd022fd0ba4d278ff314e) Fixes bug [#443512](https://bugs.kde.org/443512)
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Set Qt.ImhNoPredictiveText for text field that acts as a search field. [Commit.](http://commits.kde.org/plasma-nm/ab1ef61c2c590bc3e9ac23693ab0e2ea22e32a53) See bug [#439914](https://bugs.kde.org/439914)
+ Openconnect: Adjust version check in CMake and allow version 3.99 again. [Commit.](http://commits.kde.org/plasma-nm/ac32b9b133fff361bf3f7a61816437bc88039e50) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Hide KRunner window when pressing escape. [Commit.](http://commits.kde.org/plasma-workspace/b40ce029dcd0dea1b96c39535f40f0e92154a31e) Fixes bug [#444240](https://bugs.kde.org/444240)
+ [applets/clipboard] Fix selecting clipboard entries with Enter. [Commit.](http://commits.kde.org/plasma-workspace/fcf83e35ae241915b5b707c3f5e4a3d9d175ab55) 
+ Set Qt.ImhNoPredictiveText for text fields that act as search fields. [Commit.](http://commits.kde.org/plasma-workspace/5fe89f67d011469cad0e134a95db3d47eee78d22) See bug [#439914](https://bugs.kde.org/439914)
+ [shell] Ignore placeholder screens. [Commit.](http://commits.kde.org/plasma-workspace/7956d7b2e18133b92ec5d6cab674b1390daac9b8) See bug [#419492](https://bugs.kde.org/419492)
{{< /details >}}

