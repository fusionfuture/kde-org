2005-04-07 14:33 +0000 [r403797]  orlovich

	* kuser/propdlg.cpp: Backport the fix to the grave #100443: do not
	  scribble over uninitialized pointers, damaging account data.
	  BUG:100443

2005-04-24 13:59 +0000 [r407516]  toivo

	* kpackage/gentooInterface.cpp: Gentoo fix

2005-05-10 10:32 +0000 [r411876]  binner

	* kdeadmin.lsm: update lsm for release

2005-05-22 07:46 +0000 [r416648]  binner

	* kuser/globals.h: Increase version number after critical bugfix

