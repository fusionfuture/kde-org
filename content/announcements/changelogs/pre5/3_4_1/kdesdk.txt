2005-03-04 17:26 +0000 [r394904]  reed

	* cervisia/cvsservice/cvsaskpass.cpp, cervisia/cvsservice/main.cpp,
	  cervisia/cvsservice/Makefile.am, cervisia/main.cpp,
	  cervisia/Makefile.am: fix bus errors on Mac OS X (backport)

2005-03-04 22:17 +0000 [r394948]  dfaure

	* scripts/cvsbackport, scripts/cvsforwardport,
	  scripts/cvslastchange: Backport: don't mess up indentation when
	  backporting/forwardporting.

2005-03-05 09:08 +0000 [r395000]  okellogg

	* umbrello/umbrello/classifierlistitem.cpp,
	  umbrello/umbrello/operation.cpp, umbrello/umbrello/attribute.cpp,
	  umbrello/umbrello/interfacewidget.cpp,
	  umbrello/umbrello/attribute.h, umbrello/umbrello/enumwidget.cpp,
	  umbrello/umbrello/codegenerators/idlwriter.cpp,
	  umbrello/umbrello/dialogs/parmpropdlg.cpp,
	  umbrello/umbrello/codegenerators/idlwriter.h,
	  umbrello/umbrello/datatypewidget.cpp,
	  umbrello/umbrello/dialogs/umloperationdialog.cpp,
	  umbrello/umbrello/dialogs/umlattributedialog.cpp,
	  umbrello/umbrello/umlobject.cpp,
	  umbrello/umbrello/classifierwidget.cpp,
	  umbrello/umbrello/umlview.cpp, umbrello/umbrello/umlobject.h:
	  Synchronization with HEAD fixes bug 100290, a crash in UMLView::
	  createAutoAttributeAssociations() reported by Enrico Ros, and the
	  remainder of bug 100307. (No new features.)

2005-03-07 07:15 +0000 [r395410]  benb

	* debian/kdesdk-scripts.install, debian/kcachegrind.1,
	  debian/kbabel.shlibs, debian/kompare.install,
	  debian/catalogmanager.1, debian/fixuifiles.1 (added),
	  debian/kdesdk-scripts.README.Debian, debian/dprof2calltree.1
	  (added), debian/cervisia.xpm, debian/copyright,
	  debian/demangle.1, debian/op2calltree.1 (added),
	  debian/package_crystalsvg.1 (added), debian/control,
	  debian/png2mng.pl.1 (added), debian/pprof2calltree.1 (added),
	  debian/patches/kde-buildrc.diff (added),
	  debian/cervisia.override, debian/patches/kmtrace.diff (added),
	  debian/cvslastchange.1, debian/kdesdk-scripts.manpages,
	  debian/kdesdk-doc-html.doc-base.kcachegrind,
	  debian/kdesdk-doc-html.doc-base.cervisia, debian/kmtrace.install,
	  debian/patches/pod2man.diff, debian/extractrc.1,
	  debian/memprof2calltree.1 (added), debian/kdemangen.pl.1 (added),
	  debian/kmtrace.README.Debian (removed), debian/kmtrace.override
	  (removed), debian/libcvsservice0.shlibs, debian/cvsrevertlast.1,
	  debian/kmtrace.shlibs (removed), debian/cvsservice.1,
	  debian/poxml.README.Debian (added), debian/kbabel.1,
	  debian/patches/python.diff (removed), debian/cvsbackport.1
	  (added), debian/cvslastlog.1, debian/kdesdk-scripts.docs,
	  debian/kbugbuster.install, debian/kbabeldict.1,
	  debian/kcachegrind-converters.manpages (added),
	  debian/umbrello.xpm, debian/hotshot2calltree.1 (added),
	  debian/patches/env.diff (added), debian/changelog, debian/rules,
	  debian/kompare.1, debian/kminspector.1, debian/patches/poxml.diff
	  (added), debian/source.lintian-overrides: Sync with
	  KDE_3_3_BRANCH.

2005-03-07 15:59 +0000 [r395530]  waba

	* cervisia/repositorydlg.cpp: Fix dialog.

2005-03-07 21:59 +0000 [r395624]  benb

	* debian/patches/kde-buildrc.diff, debian/patches/kmtrace.diff,
	  debian/rules, debian/patches/poxml.diff (removed),
	  debian/patches/pod2man.diff: Bring patches up to date.

2005-03-08 22:09 +0000 [r395963]  lukas

	* scripts/kde-emacs/kde-emacs-core.el: put back the context
	  linebreak

2005-03-10 21:15 +0000 [r396469]  benb

	* debian/kdesdk-scripts.install, debian/kompare.install,
	  debian/kdesdk-misc.install, debian/cervisia.install,
	  debian/kuiviewer.install, debian/control,
	  debian/kbugbuster.install, debian/kbabel.install,
	  debian/changelog, debian/rules: Updated build-deps and install
	  files.

2005-03-10 22:05 +0000 [r396496]  benb

	* debian/kbugbuster.xpm, debian/kbabel.menu: Update menu files and
	  pixmaps.

2005-03-11 22:17 +0000 [r396826]  okellogg

	* umbrello/umbrello/dialogs/classpropdlg.cpp,
	  umbrello/umbrello/associationwidget.cpp,
	  umbrello/umbrello/dialogs/classoptionspage.cpp,
	  umbrello/umbrello/linepath.cpp,
	  umbrello/umbrello/listpopupmenu.cpp,
	  umbrello/umbrello/dialogs/classoptionspage.h,
	  umbrello/umbrello/umlview.cpp, umbrello/umbrello/linepath.h:
	  Synchronization with HEAD fixes application of diagram settings
	  to interfaces as well as listview popup and vertical relationship
	  attachments for ERDs.

2005-03-11 22:48 +0000 [r396835]  benb

	* debian/umbrello.override, debian/kompare.menu,
	  debian/kcachegrind.menu, debian/cervisia.menu,
	  debian/kompare.override, debian/kuiviewer.menu,
	  debian/kbugbuster.menu, debian/kcachegrind.override (removed),
	  debian/cervisia.override, debian/kbabel.menu,
	  debian/umbrello.menu, debian/changelog, debian/kuiviewer.override
	  (removed), debian/rules, debian/kbugbuster.override (removed),
	  debian/kbabel.override (removed): Remove obsolete kderemove tags
	  from menu items; fix old orig.tar.gz clean-up in debian/rules.

2005-03-17 21:25 +0000 [r398513]  benb

	* umbrello/umbrello/umllistview.cpp: Backport fix for incorrect
	  behaviour when moving items around in the logical tree view. See
	  KDE #57667, Debian #296433.

2005-03-18 22:56 +0000 [r398860]  benb

	* debian/kbabel.1, debian/cvs2dist.1, debian/cvsaskpass.1,
	  debian/po2xml.1, debian/umbrello.1, debian/kbabeldict.1,
	  debian/catalogmanager.1, debian/xml2pot.1, debian/transxx.1,
	  debian/copyright, debian/swappo.1, debian/kuiviewer.1,
	  debian/cvsservice.1, debian/split2po.1: Update all lists of
	  authors.

2005-03-19 01:18 +0000 [r398882]  okellogg

	* umbrello/umbrello/umllistview.cpp,
	  umbrello/umbrello/umlobject.cpp, umbrello/umbrello/umlobject.h:
	  Backport fix for bug 101148 (performance issue with large
	  models.)

2005-03-19 04:16 +0000 [r398895]  benb

	* debian/copyright: Finished license audit for kdesdk 3.4.

2005-03-19 05:35 +0000 [r398898]  jkeel

	* kompare/komparepart/komparelistview.cpp,
	  kompare/komparepart/komparesplitter.cpp,
	  kompare/komparepart/komparesplitter.h,
	  kompare/komparepart/kompareconnectwidget.cpp: BUG: 96046 Fixed an
	  accessibility bug, allowing keyboard scrolling in Kompare's main
	  difference viewer pane. The pane can now be controlled with
	  arrow/Vim-like keys, and Page Up/Down.

2005-03-19 11:24 +0000 [r398923]  okellogg

	* umbrello/umbrello/associationwidget.cpp,
	  umbrello/umbrello/linkwidget.cpp,
	  umbrello/umbrello/associationwidget.h,
	  umbrello/umbrello/linkwidget.h,
	  umbrello/umbrello/floatingtext.cpp,
	  umbrello/umbrello/messagewidget.cpp,
	  umbrello/umbrello/toolbarstatemessages.cpp,
	  umbrello/umbrello/messagewidget.h: Backport fix for bug 53376.

2005-03-19 22:27 +0000 [r399076]  benb

	* debian/control, debian/changelog: Fix kdepim build-depends.

2005-03-19 23:00 +0000 [r399093]  okellogg

	* umbrello/umbrello/umllistview.cpp: Backport following fix from
	  HEAD: > CVS commit by okellogg: > popupMenuSel(mt_Delete):
	  UMLDoc::removeObject() may physically delete the > object, hence
	  call UMLCanvasObject::removeAllAssociations() beforehand.

2005-03-19 23:29 +0000 [r399102-399101]  benb

	* debian/control, debian/changelog: Update pim-related
	  build-depends again.

	* debian/umbrello.docs, debian/rules: Update miscellaneous docs.

2005-03-20 00:14 +0000 [r399105]  benb

	* debian/kbabel.shlibs, debian/libcvsservice0.shlibs, debian/rules,
	  debian/kompare.override (removed): Update shlibs files and
	  overrides.

2005-03-20 06:40 +0000 [r399129]  benb

	* debian/cvsaskpass.1, debian/cvsbackport.1, debian/cvslastlog.1,
	  debian/cvsforwardport.1 (added), debian/cvslastchange.1,
	  debian/kdesdk-scripts.manpages, debian/cvsrevertlast.1: Update
	  some manpages and add the new cvsforwardport.1.

2005-03-20 07:04 +0000 [r399132-399131]  benb

	* debian/patches/env.diff: Replace more uses of /usr/bin/env.

	* debian/create_cvsignore.1, debian/fixuifiles.1,
	  debian/cvs-clean.1: Some more manpage updates.

2005-03-20 07:33 +0000 [r399135]  benb

	* debian/cvslastchange.1, debian/makeobj.1, debian/cvscheck.1: Yet
	  more manpage updates.

2005-03-20 11:29 +0000 [r399166-399165]  benb

	* debian/extractattr.1 (added), debian/kdesdk-scripts.manpages,
	  debian/extractrc.1: More manpages.

	* debian/control, debian/changelog: Shift around versioned cvs
	  dependencies.

2005-03-20 12:08 +0000 [r399172]  benb

	* debian/cvs2dist.1, debian/split2po.1: More options for cvs2dist
	  and environment variables for split2po. This finishes the manpage
	  updates for kdesdk/scripts and kdesdk/poxml.

2005-03-21 21:52 +0000 [r399578]  weidendo

	* kcachegrind/kcachegrind/callgraphview.cpp: Backport of fix for
	  bug 101556 (Use "long" for node IDs in graphs)

2005-03-23 22:05 +0000 [r400109]  thiago

	* scripts/svn2dist (added), scripts/svnlastlog (added),
	  scripts/create_svnignore (added), scripts/nonsvnlist (added),
	  scripts/svnversions (added), scripts/svnlastchange (added),
	  scripts/svnaddcurrentdir (added), scripts/svncheck (added),
	  scripts/colorsvn (added), scripts/svngettags (added),
	  scripts/svn-clean (added): These are the CVS scripts ported to
	  Subversion. They should behave just like their CVS counterparts,
	  for people who are used to them. However, please note that some
	  differences are expected, so be aware. Also, note that these
	  scripts need stress-testing. I have done my best to make sure
	  they work (in most cases), but please test them as well. Report
	  to me any problems if you can't fix them yourself. Do not use
	  these scripts in production environment unless you know what
	  you're doing, or until they have been tested. You have been
	  warned. colorsvn: counterpart to colorcvs and shares its config
	  file as well. Colourises the svn output so that additions,
	  modifications, updates, rejects, etc. are marked by colour. Known
	  bug: perl won't display stderr for some commands ("commit"
	  commands) create_svnignore: counterpart to create_cvsignore. Does
	  the same things, but instead of writing to .cvsignore, adds to
	  the svn:ignore property. nonsvnlist: counterpart to noncvslist:
	  lists all files not in the repository (it's basically just svn st
	  | grep '^\?') svn-clean: counterpart to cvs-clean: it is used to
	  clean up your working dir of files that are not in the repository
	  svn2dist: counterpart to cvs2dist. Completely untested, and won't
	  work until we define the Subversion repository structure and
	  where the admin dir will be svnaddcurrentdir: counterpart to
	  cvsaddcurrentdir: adds the files in the current dir to the
	  Subversion repository. The directory itself is not added, and it
	  is, in fact, expected to be a Subversion checkout already
	  svncheck: superfluous script, provided only for convenience for
	  people who are used to cvscheck. It will annoy you saying what
	  command you should be using (svn status) svngettags: superfluous
	  script, it's only there to tell you tags don't exist in
	  Subversion svnlastchage: superfluous script, provided only for
	  convenience for people who are used to cvslastchange. It will
	  annoy you saying what command you should be using svnlastlog:
	  superfluous script as well (same comments) svnversions: lists the
	  revision a file was last modified CCMAIL:kde-core-devel@kde.org

2005-03-23 22:20 +0000 [r400115]  thiago

	* scripts/svn2dist (removed), scripts/svnlastlog (removed),
	  scripts/create_svnignore (removed), scripts/nonsvnlist (removed),
	  scripts/svnversions (removed), scripts/svnlastchange (removed),
	  scripts/svnaddcurrentdir (removed), scripts/svncheck (removed),
	  scripts/colorsvn (removed), scripts/svngettags (removed),
	  scripts/svn-clean (removed): Removing the files from the 3.4
	  branch, that I committed to by accident. I am terribly sorry.

2005-03-23 22:36 +0000 [r400124]  okellogg

	* umbrello/umbrello/linkwidget.cpp, umbrello/umbrello/linkwidget.h,
	  umbrello/umbrello/messagewidget.cpp: Backport following fix from
	  HEAD: > CVS commit by okellogg: > LinkWidget::getOperationText():
	  Fix crash on loading sequence messages.

2005-03-24 22:49 +0000 [r400350]  benb

	* debian/changelog, debian/poxml.README.Debian: Merge changes from
	  the recent 3.3.2 upload to sid.

2005-03-25 00:58 +0000 [r400382]  benb

	* debian/kbabel.1, debian/control, debian/umbrello.1,
	  debian/kcachegrind.1, debian/kbabeldict.1,
	  debian/catalogmanager.1, debian/kuiviewer.1, debian/kompare.1:
	  Final description and manpage updates for 3.4.

2005-03-25 01:57 +0000 [r400383]  benb

	* debian/changelog: Oh. Yeah, changelog dates should be
	  approximately monotonic.

2005-03-25 21:01 +0000 [r400613]  weidendo

	* kcachegrind/kcachegrind/kwidgetaction.cpp (removed),
	  kcachegrind/kcachegrind/kwidgetaction.h (removed),
	  kcachegrind/kcachegrind/Makefile.am,
	  kcachegrind/kcachegrind/toplevel.cpp: Backport: Get rid of
	  unneeded actions. Search was moved to function profile. This
	  fixes a bug with the "Search:" KWidgetAction widget, which is
	  shown in the top left corner if not plugged into a toolbar (?).

2005-03-25 23:09 +0000 [r400654]  okellogg

	* umbrello/umbrello/refactoring/refactoringassistant.cpp,
	  umbrello/umbrello/linkwidget.cpp, umbrello/umbrello/enum.cpp,
	  umbrello/umbrello/operation.h, umbrello/umbrello/class.cpp,
	  umbrello/umbrello/linkwidget.h,
	  umbrello/umbrello/floatingtext.cpp, umbrello/umbrello/enum.h,
	  umbrello/umbrello/class.h, umbrello/umbrello/entity.cpp,
	  umbrello/umbrello/floatingtext.h,
	  umbrello/umbrello/umlcanvasobject.cpp,
	  umbrello/umbrello/associationwidget.cpp,
	  umbrello/umbrello/entity.h, umbrello/umbrello/umldoc.cpp,
	  umbrello/umbrello/classifier.cpp,
	  umbrello/umbrello/umllistview.cpp,
	  umbrello/umbrello/associationwidget.h,
	  umbrello/umbrello/umlcanvasobject.h,
	  umbrello/umbrello/messagewidget.cpp, umbrello/umbrello/umldoc.h,
	  umbrello/umbrello/umlview.cpp, umbrello/umbrello/classifier.h,
	  umbrello/umbrello/messagewidget.h: Synchronization with HEAD
	  gives promotion of operation renaming from the list view to
	  sequence and collaboration messages, plus various design
	  cleanups.

2005-03-26 09:42 +0000 [r400707]  benb

	* debian/cvscheck.1: More detail on --local.

2005-03-28 11:18 +0000 [r401227]  cloose

	* cervisia/cervisiapart.cpp, cervisia/cervisiapart.h,
	  cervisia/ChangeLog: backport from HEAD: Fix BR #97664 - statusbar
	  when embedded in Konqueror.

2005-03-30 19:40 +0000 [r401990]  cloose

	* cervisia/mergedlg.h, cervisia/progressdlg.cpp,
	  cervisia/settingsdlg.h, cervisia/protocolview.cpp,
	  cervisia/cervisiapart.cpp, cervisia/diffview.cpp,
	  cervisia/diffdlg.h, cervisia/globalignorelist.h,
	  cervisia/resolvedlg.cpp, cervisia/cvsinitdlg.cpp,
	  cervisia/watchersdlg.cpp, cervisia/annotateview.h,
	  cervisia/entry_status.h, cervisia/ignorelistbase.cpp,
	  cervisia/updateview.cpp, cervisia/updateview_items.cpp,
	  cervisia/addrepositorydlg.cpp, cervisia/annotatectl.h,
	  cervisia/changelogdlg.h, cervisia/updateview_visitors.h,
	  cervisia/mergedlg.cpp, cervisia/loginfo.cpp, cervisia/tooltip.h,
	  cervisia/progressdlg.h, cervisia/globalignorelist.cpp,
	  cervisia/logtree.cpp, cervisia/entry.cpp, cervisia/checkoutdlg.h,
	  cervisia/entry_status.cpp, cervisia/annotateview.cpp,
	  cervisia/logplainview.h, cervisia/ChangeLog, cervisia/diffview.h,
	  cervisia/updatedlg.cpp, cervisia/watchersdlg.h,
	  cervisia/annotatectl.cpp, cervisia/loglist.cpp,
	  cervisia/changelogdlg.cpp, cervisia/stringmatcher.h,
	  cervisia/commitdlg.cpp, cervisia/updateview.h,
	  cervisia/dirignorelist.h, cervisia/addrepositorydlg.h,
	  cervisia/resolvedlg_p.cpp, cervisia/repositories.cpp,
	  cervisia/tooltip.cpp, cervisia/misc.cpp, cervisia/LICENSE.QPL
	  (removed), cervisia/checkoutdlg.cpp, cervisia/watchdlg.cpp,
	  cervisia/addremovedlg.cpp, cervisia/cervisiashell.cpp,
	  cervisia/logplainview.cpp, cervisia/loginfo.h,
	  cervisia/annotatedlg.h, cervisia/entry.h, cervisia/logtree.h,
	  cervisia/tagdlg.h, cervisia/stringmatcher.cpp,
	  cervisia/repositorydlg.h, cervisia/editwithmenu.h,
	  cervisia/logdlg.h, cervisia/updatedlg.h,
	  cervisia/dirignorelist.cpp, cervisia/patchoptiondlg.cpp,
	  cervisia/loglist.h, cervisia/commitdlg.h, cervisia/COPYING,
	  cervisia/settingsdlg.cpp, cervisia/resolvedlg_p.h,
	  cervisia/repositories.h, cervisia/historydlg.h,
	  cervisia/cvsdir.h, cervisia/diffdlg.cpp, cervisia/misc.h,
	  cervisia/annotatedlg.cpp, cervisia/cervisiapart.h,
	  cervisia/protocolview.h, cervisia/cervisiashell.h,
	  cervisia/addremovedlg.h, cervisia/tagdlg.cpp,
	  cervisia/watchdlg.h, cervisia/editwithmenu.cpp,
	  cervisia/repositorydlg.cpp, cervisia/logdlg.cpp,
	  cervisia/cvsinitdlg.h, cervisia/resolvedlg.h,
	  cervisia/entry_status_change.h, cervisia/ignorelistbase.h,
	  cervisia/updateview_visitors.cpp, cervisia/updateview_items.h,
	  cervisia/patchoptiondlg.h, cervisia/main.cpp,
	  cervisia/historydlg.cpp, cervisia/cvsdir.cpp: backport from HEAD:
	  Changed license from QPL to GPL v2 or later. Reference:
	  http://lists.kde.org/?l=kde-core-devel&m=110929873315662&w=2
	  Accepted by the main contributors: Andre Woebbeking -
	  http://lists.kde.org/?l=kde-core-devel&m=110936467717024&w=2
	  Bernd Gehrmann -
	  http://lists.kde.org/?l=kde-core-devel&m=111113902322727&w=2
	  Christian Loose -
	  http://lists.kde.org/?l=kde-core-devel&m=110935555905931&w=2
	  Florent Pillet -
	  http://lists.kde.org/?l=kde-core-devel&m=111122853023456&w=2
	  Richard Moore -
	  http://lists.kde.org/?l=kde-core-devel&m=110933883604003&w=2

2005-03-31 10:15 +0000 [r402103]  benb

	* kbabel/kbabel/kbhighlighting.cpp: Replace check() with
	  checkWord(). This appears to prevent situations in which kbabel
	  can crash without warning during on-the-fly spellchecking (in
	  particular, for a long entry with lots of spelling errors). Patch
	  posted to kde-core-devel on 25 March, no objections raised. See
	  http://bugs.debian.org/289646 for further details.

2005-03-31 10:25 +0000 [r402108]  benb

	* scripts/kdemangen.pl: Fix output in UTF-8 locales by using minus
	  instead of hyphen. Thanks to Achim Bohnet for the patch.

2005-03-31 10:33 +0000 [r402112]  benb

	* debian/changelog, debian/copyright: Note what we did in
	  KDE_3_3_BRANCH.

2005-03-31 11:56 +0000 [r402138]  benb

	* debian/changelog: Another bug fixed in KDE_3_3_BRANCH, resync.

2005-03-31 19:56 +0000 [r402247]  okellogg

	* umbrello/umbrello/umldoc.cpp, umbrello/umbrello/uml.cpp: Backport
	  following fix from HEAD: > CVS commit by okellogg: > Patch by
	  Albert Astals Cid fixes a saving error described in >
	  http://sourceforge.net/mailarchive/forum.php?thread_id=6923476&forum_id=472

2005-04-01 16:00 +0000 [r402433]  marchand

	* kioslave/svn/svn.cpp: backport URLs cleaning fix

2005-04-04 05:30 +0000 [r403024-403023]  okellogg

	* umbrello/umbrello/umlview.cpp: Backport patch by Lutz Mueller for
	  bug 103133.

	* umbrello/umbrello/dialogs/classifierlistpage.cpp,
	  umbrello/umbrello/entity.h, umbrello/umbrello/classifier.cpp,
	  umbrello/umbrello/dialogs/classifierlistpage.h,
	  umbrello/umbrello/enum.cpp, umbrello/umbrello/class.cpp,
	  umbrello/umbrello/classifier.h, umbrello/umbrello/enum.h,
	  umbrello/umbrello/class.h, umbrello/umbrello/entity.cpp: Backport
	  fix for bug 103123 (change order of operations in class diagram)

2005-04-06 20:24 +0000 [r403610-403609]  nanulo

	* kbabel/catalogmanager/catalogmanager.cpp: propagate changes from
	  project dialog (backport)

	* kbabel/VERSION: bump version

2005-04-08 11:50 +0000 [r404038]  je4d

	* kompare/main.cpp: backport from kompare3_branch: Gracefully
	  ignore -e, rather than choking on it.

2005-04-08 20:32 +0000 [r404148]  okellogg

	* umbrello/umbrello/messagewidget.cpp,
	  umbrello/umbrello/messagewidget.h, umbrello/umbrello/umlview.h:
	  Backport fix for reoccurrences of bug 57875 (sequence diagram
	  label placement)

2005-04-12 22:05 +0000 [r405179]  okellogg

	* umbrello/umbrello/association.h,
	  umbrello/umbrello/codegenerators/javawriter.cpp,
	  umbrello/umbrello/codegenerators/xmlschemawriter.cpp,
	  umbrello/umbrello/codegenerators/cppwriter.cpp,
	  umbrello/umbrello/codegenerators/classifierinfo.cpp,
	  umbrello/umbrello/codegenerators/adawriter.cpp,
	  umbrello/umbrello/codegenerators/phpwriter.cpp,
	  umbrello/umbrello/codegenerators/idlwriter.cpp,
	  umbrello/umbrello/association.cpp,
	  umbrello/umbrello/codegenerators/php5writer.cpp: Backport fix for
	  bug 103728.

2005-04-16 15:22 +0000 [r405914]  okellogg

	* umbrello/ChangeLog, umbrello/umbrello/messagewidget.cpp: Backport
	  fix for bug 101541 from HEAD > onWidget(): > Fix to
	  {top,bottom}ArrowY brings on the properties dialog upon double
	  clicking

2005-04-17 15:02 +0000 [r406086]  okellogg

	* umbrello/umbrello/classparser/parser.cpp: Backport following fix
	  from HEAD: > CVS commit by okellogg: > parseEnumSpecifier(): Skip
	  comment after last enum literal. > Thanks to Braddock Gaskill for
	  reporting the problem.

2005-04-19 21:42 +0000 [r406649]  okellogg

	* umbrello/umbrello/toolbarstate.cpp: Backport fix for bug 89691: >
	  mouseMove(): Don't base canvas scroll on the
	  inverseWorldMatrix().

2005-04-26 20:05 +0000 [r408028]  okellogg

	* umbrello/umbrello/clipboard/umldrag.h,
	  umbrello/umbrello/clipboard/umlclipboard.h,
	  umbrello/umbrello/umldoc.cpp, umbrello/umbrello/classifier.cpp,
	  umbrello/umbrello/umllistview.cpp, umbrello/ChangeLog,
	  umbrello/umbrello/umldoc.h, umbrello/umbrello/umlview.cpp,
	  umbrello/umbrello/umllistview.h,
	  umbrello/umbrello/clipboard/umldrag.cpp,
	  umbrello/umbrello/package.cpp,
	  umbrello/umbrello/clipboard/umlclipboard.cpp,
	  umbrello/umbrello/uml.cpp, umbrello/umbrello/umlview.h: Backport
	  fix for bug 70924 from HEAD.

2005-04-28 04:42 +0000 [r408306]  okellogg

	* umbrello/umbrello/umlview.cpp, umbrello/umbrello/umlview.h:
	  Backport following fix from HEAD: > {set,get}Name(): Revert the
	  change of umlview.h:1.66 and umlview.cpp:1.198, > it spoiled
	  non-latin1 diagram names. > Thanks to Andre Woebbeking
	  <Woebbeking_AT_onlinehome.de> for spotting the > problem.

2005-04-30 04:35 +0000 [r408743]  okellogg

	* umbrello/umbrello/codegenerators/cppheadercodeoperation.cpp:
	  Backport following fix from HEAD: > updateMethodDeclaration():
	  Generate "static" keyword if required. > Thanks to Jay Snyder for
	  reporting the problem.

2005-05-10 10:32 +0000 [r411876]  binner

	* kdesdk.lsm: update lsm for release

2005-05-10 10:44 +0000 [r411886]  jriddell

	* umbrello/VERSION: Increase version number to 1.4.1 for kde 3.4.1

2005-05-13 05:32 +0000 [r413016]  okellogg

	* umbrello/umbrello/interfacewidget.cpp: Backport SVN commit
	  412607, fix for bug 104637: > drawAsConcept(): Use
	  UMLWidget::getFontMetrics() for fontHeight determination.

2005-05-15 05:50 +0000 [r414018]  benb

	* debian/control, debian/changelog, debian/rules: New 3.4 upload to
	  experimental. Merge changes from 3.3.x, disable CVS rules,
	  conflict with kdevelop3-plugins while both provide the svn
	  ioslave.

2005-05-18 18:19 +0000 [r415492]  okellogg

	* umbrello/ChangeLog: update beast kill stats

2005-05-18 21:05 +0000 [r415551]  okellogg

	* umbrello/ChangeLog: update beast kill stats

2005-05-22 08:02 +0000 [r416655]  binner

	* kompare/main.cpp: version++

