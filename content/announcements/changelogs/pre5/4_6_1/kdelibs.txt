commit acf6b58892575b95255fffd93162bcb856260fbb
Author: Sebastian Trueg <trueg@kde.org>
Date:   Fri Feb 25 09:41:11 2011 +0100

    Better genericLabel() which uses the full nie:url for non-local URLs like web pages.

commit 97535833a66f32e14b5529b8b50bba8beef18967
Merge: ac5d2ab d1c4ed0
Author: Björn Ruberg <bjoern@ruberg-wegener.de>
Date:   Thu Feb 24 20:23:56 2011 +0100

    Merge branch 'KDE/4.6' of git://anongit.kde.org/kdelibs into KDE/4.6

commit ac5d2abf45b238ae9f25985ff282f785e148014d
Author: Björn Ruberg <bjoern@ruberg-wegener.de>
Date:   Thu Feb 24 20:22:06 2011 +0100

    Properly restore the size of PopupApplets from last session even when a preferredSize is set in the graphicsWidget()
    BUG: 236853
    BUG: 233235
    
     Please enter the commit message for your changes. Lines starting

commit d1c4ed048b57ae8fb324e185086c12f65e2648e4
Author: Marco Martin <notmart@gmail.com>
Date:   Sun Feb 20 16:51:16 2011 +0100

    scripted containment loading fix
    
    load as containment also applets that have both Plasma/Applet and Plasma/Containment

commit 932a93ba927705d90155f15e67394ef6732dec3b
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Feb 24 16:59:08 2011 +0100

    bump version

commit 65670a8327ee270ef01db8b97f8c2b8799ad7bb8
Author: David Faure <faure@kde.org>
Date:   Thu Feb 24 17:01:16 2011 +0100

    Fix crash when following a link to a multipart/mixed url
    
    componentData() returned an invalid KComponentData, for lack of a name
    passed to the ctor.

commit 79d90b82b6e9d097fba62b67ad353bffaba68000
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Feb 14 11:19:56 2011 +0100

    refactoring and cleanup of ProcessList class and related functions
    
    (backported from master commit e0ad4dae130d574547c7753d0b555e8a64e600a9)

commit 6d49c507672803d826337e23c8c654e9cd7ebf5e
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Feb 14 00:31:06 2011 +0100

    refactored Kuser usage - it is enough to use one instance
    
    (backported from master commit 7c230c2cebf81eeb5659a982385043bc2cd82782)

commit a1fda41bb34ec9660cf98684ba3089e6bf68d92f
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Sun Feb 13 19:32:15 2011 +0100

    kdeinit on win32 has to be a console app, otherwise stderr catching will be broken
    
    (backported from master commit 55f80e23cf1cc88b06884d76241efa360bb54765)

commit 97d915da924dcae84751be79dfc862ff0f61704d
Author: Lukas Tinkl <lukas@kde.org>
Date:   Wed Feb 23 18:21:37 2011 +0100

    Do not filter out /dev/. directories
    
    They are used for system daemons these days, like systemd
    (http://www.freedesktop.org/wiki/Software/systemd)
    
    BUG: 266973

commit 92168da7d28b0bcef1d0271bacd3bf356fc9d68f
Author: Lukas Tinkl <lukas@kde.org>
Date:   Tue Feb 22 18:17:13 2011 +0100

    make the internal property cache... uhm work
    
    For the long story, compare:
    QVariant UDisksDevice::property(const QString &key) const
     and
    QVariant QObject::property(const char *name) const;
    
    and wonder why the cache never sped up anything, the wrong code path
    (the latter) has always been hit

commit 43823ec863245eed8ba203a50f1e03830629bcab
Author: Sebastian Trueg <trueg@kde.org>
Date:   Tue Feb 22 14:02:18 2011 +0100

    Close the query after listing in blocking mode to match behaviour
    as described in the API documentation.

commit cfaa411382b2c2f8e965bca2dbdf9d6f31b65e29
Author: David Faure <faure@kde.org>
Date:   Tue Feb 22 08:50:56 2011 +0100

    Fix autorename feature so that it calls suggestName() correctly.
    
    It was moving files into other directories unexpectedly.
    Based on toddrme's patch, but also handles other cases (same partition
    vs different partition), and a fix for non-interactive mode in order to
    test this feature from a unittest (trunk only, needs new API)
    
    BUG: 256650
    FIXED-IN: 4.6.1
    (cherry picked from commit fa1e90f4ba42f1267c12d1a28de3acf9fcbebb01)

commit abc9c4c67cf5a25d0f279beedc81727cca154cad
Author: Frank Reininghaus <frank78ac@googlemail.com>
Date:   Mon Feb 21 19:29:47 2011 +0100

    KListWidgetSearchLine: filter items that are inserted or changed
    
    Up to now, the search line only filters items that are in the widget at
    the time a search is started. With this commit, items that are added
    or modified later are filtered as well. Unit test included

commit e3b0aed5bf72858b772577f8b6688b9c7085bca2
Author: David Faure <faure@kde.org>
Date:   Mon Feb 21 20:35:52 2011 +0100

    Forwardport support for oom_score_adj (kernel >= 2.6.36) from trinity branch
    
    CCMAIL: kb9vqf@pearsoncomputing.net
    (cherry picked from commit 1bea60f843dfe098b1a411482b7e404bfe8a0e55)

commit 55afceaa92baee309bf9cc661c57a240e866b71a
Author: Lukas Tinkl <lukas@kde.org>
Date:   Sun Feb 20 19:46:17 2011 +0100

    fix icons for an empty optical drive and a Video/SuperVideo CD

commit 8f8627e957a90dcdb98c70d4f5f62bf919700b99
Author: Frederik Schwarzer <schwarzerf@gmail.com>
Date:   Sun Feb 20 15:45:58 2011 +0100

    fix wording of checkbox label

commit 4c075143e4dc6a2a169c64a154f2d68ba17b5900
Author: John Layt <john@layt.net>
Date:   Thu Feb 17 13:08:02 2011 +0000

    Fix Indian National calendar system days in month
    
    Only months 2 to 6 are 31 days

commit 0e6adca725db7f4baf73e7eaf224f47c7c33165f
Author: Lukas Tinkl <ltinkl@redhat.com>
Date:   Tue Feb 15 17:21:10 2011 +0100

    don't try to construct the label too hard
    
    return an empty label instead, as per docs; fixes eg. kio_sysinfo
    
    BUG: 266359

commit 10f3c35073ff8756ab9e55aab86621a7008b6928
Author: David Faure <faure@kde.org>
Date:   Tue Jan 11 19:41:54 2011 +0000

    Explain in kdebug.h why people shouldn't use a global initializer
    
    (Partial backport of 6d3bcd84b8ee97bb36b9cf3217fe7152fee7f62b to 4.6 branch)

commit 21206aaf49fa7c57681587a864260d8e335a7d1e
Author: David Faure <faure@kde.org>
Date:   Wed Dec 8 13:35:14 2010 +0000

    Further fixes to kDebug:
    
    * Since KConfig works without a main componentdata nowadays (auto-created by kglobal), make kDebug work
    pretty much the same in qt-only apps as in kde apps, including obeying kdebugrc settings. The only difference
    is that area 0 (and unknown areas) are called qAppName() rather than main-component-data-name.
    * Update that name when the main component data gets created, even after early kDebugs.
    * Make the handling of unknown numbered areas more consistent (using the settings for area 0)
    * Complete unittest for the "no component data" case, in a separate program called by kdebug_unittest.
    
    Backport of 35386117c0d35c1f9f1d787a46a3d4dcde5ef160 to 4.6 branch

commit afbd365c14ae581daa279df3059c4b43e0b964e2
Author: David Faure <faure@kde.org>
Date:   Tue Dec 7 13:38:31 2010 +0000

    Fix assert when dynamicArea is used before numbered area, in the case where there's no mainComponentData.
    
    Backport of 09d1b660810bdfbd7e3beba638c54f4f5942cbe3 to 4.6 branch

commit 5c17897997a8dc5194ccb2d4247cb608cc6f9734
Author: David Faure <faure@kde.org>
Date:   Mon Dec 6 19:00:32 2010 +0000

    Fix debug output from klauncher-started apps being associated with debug area "kde"
    
    (the default component data name), due to kDebug() being used before the main
    component data is created (e.g. in kdeinit4's call to KStartupInfo::createNewStartupId).
    
    Backport of 7bd9bb446e7b261017c602c28ce85b8d829d399e to 4.6 branch

commit b1ff9486a2aa0fb5b65313c93f6715172375c120
Author: David Faure <faure@kde.org>
Date:   Mon Sep 27 13:42:34 2010 +0000

    Fix crash when using 2 dynamic areas before using a numbered area: the numbered area definitions were not being loaded.
    
    Happened in kpasswdservertest (in kdebase), now unittested explicitely here as well.
    
    Commit acea5ede4f6e0ff39b6186d5170ec9934ab79a8e (r1180264) backported to 4.6 branch.

commit d2371b370aab76d940ea2559e35cd04fb3d960fb
Author: David Faure <faure@kde.org>
Date:   Wed Sep 15 18:54:10 2010 +0000

    Use the fake component data when there isn't a real one, so that kDebug works in qt-only apps, but can be deactivated in kdebugdialog.
    
    Commit 4028b8f64de50d5e1e7c3c4f7ba6ca5ad59d72cf (r1175739) backported to 4.6 branch.

commit 85450f295285d81364a4fb912bb9ecfdc15d8d58
Author: David Faure <faure@kde.org>
Date:   Fri Sep 3 12:27:14 2010 +0000

    Fix debug output from KSharedDataCache appearing in every Qt-only app I launch
    
    even though that area is disabled in kdebugrc [which isn't found].
    
    Commit 3601734513e31e20288fca470b795596fd906bcb backported to 4.6 branch

commit d2869f70dafe0b45fade7b256ac267f492f62507
Author: David Faure <faure@kde.org>
Date:   Tue Feb 15 01:44:32 2011 +0100

    Fix ASSERT: "lister->d->m_cachedItemsJob == 0" when using KDirModel::expandTo(/a/b/c) and
    /a and /a/b are in cache already; when the CachedItemsJob for /a emits its items,
    KDirModel sees b and asks for listing /a/b, which creates another CachedItemsJob for /a/b.
    So it's perfectly possible to have more than one cached items job in a dirlister, just
    not for the same directory -> turned the job member into a list of jobs. With unittest.
    FIXED-IN: 4.6.1
    BUG: 175035
    (cherry picked from commit 4530a69919a97edd87228882fe72b2759ec4dc29)

commit 336abf509a24aed283c084967b946eb94a229834
Author: David Faure <faure@kde.org>
Date:   Thu Feb 10 16:55:39 2011 +0100

    Minor: outline CachedItemsJob constructor
    (cherry picked from commit 2e5e9985db3c223fc10f61c7e5840092cb80b967)

commit bef0bd3e3ff340ad72cc757df891cbd62560f2de
Author: David Faure <faure@kde.org>
Date:   Wed Feb 9 17:39:36 2011 +0100

    Fixed assert when the user stops the running list job.
    
    Testcase: `dolphin /`, Ctrl+F, *.h, press stop button in status bar.
    The code said "leave the job running, other views might be interested,
    and the job is just like any other update job", but this use case shows
    that this isn't wanted (and there are no update jobs for kio_filenamesearch).
    
    Implementation details: jobs are now killed using kill() in order to
    trigger the real implementation of slotResult/slotUpdateResult; added
    a doKill for implementing it in CachedItemsJob. Less duplication overall.
    
    Added unittest about deleting a KDirLister before it has time to emit items,
    as found during dolphin startup.
    (cherry picked from commit 799e3c6a5842afc463b975b5276baee4bd11d5aa)

commit cb0e915925fc8a854c31b416b33f43e794f11119
Author: Till Adam <till@kdab.com>
Date:   Sun Feb 6 20:48:27 2011 +0100

    Make the KProcess mode for KIO also usable on OSX and use it.
    
    On OSX, as of 10.6, one cannot fork with out exec'ing, more or less,
    which makes the way kdeinit works problematic. In order not to introduce
    yet another code path, this makes the so far Windows specific way of
    handling kio processes via KProcess, instead of kdeinit available
    more generically and uses it on OSX as well. This is based on
    proof of concept patches by David Faure.
    
    CCBUG: 209903
    (cherry picked from commit 19f4bf0c212a28e79ef9b8d0cb35068951e58a85)

commit 50460416cd08d2b4457cfa2d1650ccebd076ad6c
Author: Till Adam <till@kdab.com>
Date:   Sat Feb 5 21:54:30 2011 +0100

    Determine docpath and bundlepath (on OSX) before forking.
    
    If not done in the original process, these lead to crashes on OSX.
    (cherry picked from commit 1a600e5f120c2f15858e7cdbe8d02b314bfe818a)

commit 5d84e2d1c521e9bf60dfc4cbd29111c4d074dc67
Author: Till Adam <till@kdab.com>
Date:   Sat Feb 5 21:55:35 2011 +0100

    Unify whitespace.
    (cherry picked from commit 59315c3c49eb0c4f803b712b11c243c5ac6cdcc0)

commit aa2eb4d1745a63b31c0d000faa4dd832b812d1c4
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Feb 14 22:27:11 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 7daa877651c5d0179435ead49019ba6f9dec4fbb
Author: Sebastian Trueg <trueg@kde.org>
Date:   Tue Jan 11 13:39:16 2011 +0100

    Yet another strange optimization: When using "?r nao:userVisible 1" to restrict to visible results the Virtuoso optimizer does treat this pattern with a high priority, using it to preselect results or something like that. This is not what we want. Since the big part of the resources is in fact visible we want that pattern to be used as a filter at the end of the query. The only way to force this (besides bypassing the Virtuoso query optimizer which would be a bad idea) is to use a filter that is not an equality: "?r nao:userVisible ?v . FILTER(?v>0)"

commit e3067adf4ed465ebc3aca78c882501a3f2605adb
Author: Sebastian Trueg <trueg@kde.org>
Date:   Sat Nov 20 11:41:37 2010 +0100

    Two substantial performance improvements based on the new CrappyInferencer2 in the storage service in kdebase: 1. We now have type inference and can use "?r a <type>" directly without using crappy inference. 2. Crappy inferencer2 creates a nao:userVisible property for all resources. Thus, again there is no need for crappy inference1 like conditions that are very slow. Instead we can directly check the visiblitly on the results.

commit f1b615d6d6e7ef066ed8585bf5537434a23c629a
Author: Andreas Hartmetz <ahartmetz@gmail.com>
Date:   Mon Feb 14 15:03:40 2011 +0100

    Deduplicate certificates returned by Qt.
    
    This should fix assertion failures when manipulating the list of
    certficates in the SSL KCM.

commit c2dc7faf0366926ecc44da22b607388a6b267c33
Author: Andreas Hartmetz <ahartmetz@gmail.com>
Date:   Mon Feb 14 05:58:59 2011 +0100

    Don't set uniform row heights; checkable rows are higher.

commit 0e55f6d283ec2ffa0638780d06c921e7c0faa5ab
Author: John Layt <john@layt.net>
Date:   Sun Feb 13 18:54:15 2011 +0000

    Fix failing Calendar Tests
    
    Fix calendar test which has result that changes every year, and fix
    tests using the wrong format length.

commit b1abd618dcff0920a60631296c560884eb2d4930
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Fri Feb 11 21:58:46 2011 +0100

    Fix issue that no previews are shown when searching for files
    
    Based on the patch provided by bbloldd at bug 264005
    
    BUG: 264005
    FIXED-IN: 4.6.1

commit 6e208cf0cc60d3e7016a3bbdbb6dfd3c1b105186
Author: Lasse Liehu <lliehu@kolumbus.fi>
Date:   Thu Feb 10 21:01:29 2011 +0200

    ignore() key press event for keys not handled

commit 9e5b82194198a517ce2d5f7777e10e1f5ca0c9d0
Author: Till Adam <adam@kde.org>
Date:   Mon Sep 27 13:31:48 2010 +0000

    Provide an implementation of match() that forwards to the source model properly.
    
    svn path=/branches/KDE/4.5/kdelibs/; revision=1180254

commit 6a4bf6e507e5a084ab52a5a7ced1a8cbb70187a8
Author: Marco Martin <notmart@gmail.com>
Date:   Wed Feb 9 19:17:44 2011 +0100

    correct the text used to fugure out the button
    
    button have text like &Ok &Yes
    This way to detect what button was pressed of course is horrible and will have to be changed
    CCBUG:265738

commit c9256561d74c9e890c061f4c289a4437a125953d
Author: Marco Martin <notmart@gmail.com>
Date:   Wed Feb 9 00:02:28 2011 +0100

    emit cancel signal
    
    both when clicking cancel and pressing esc
    CCBUG:265738

commit 0126c4bf738229fedb1a5a1c05abf565b3131dce
Author: Andrea Iacovitti <aiacovitti@libero.it>
Date:   Mon Feb 7 21:57:06 2011 +0100

    Check for parent() to avoid crash
    
    BUG: 264985

commit 065ab288a86804e109e4886a07b1de61788135ab
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Feb 6 11:02:56 2011 -0500

    If only "*" is left after ignoring the options in a rule, then disregard the ad
    block filter to avoid creating a wildcard match that will filter every request.

commit 7284d452badac7bc4276842cfbade6a51442c6a6
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sat Feb 5 13:34:00 2011 -0200

    Backport 57c4c504504f3a194a12971080e52afa9218577d by lvsouza from master
    to the 4.6 branch:
    
    Treats UPS (uptype == 3) as batteries.
    Powerdevil still does not treats my UPS as battery but
    solid-hardware query 'IS Battery' works after this change.
    It is a start.
    
    CCBUG: 197627

commit fe20b2bb1c275cb081c5d19e25a17f9e46cf80b9
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri Feb 4 17:51:12 2011 -0800

    save/track/restore ItemStatus properly

commit 7a184d8abab4eff38b1bb1649b8c37c44ca070fe
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri Feb 4 16:18:03 2011 -0800

    toggle on activation

commit 7ca7e81303c50769e286897be0afe0793dabdf52
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Feb 4 18:29:51 2011 -0500

    Workaround for the hang (freeze) when opening VLC's file dialog under KDE.
    See http://git.reviewboard.kde.org/r/100539/ for the details.
    
    BUG:260719
    REVIEW:100539

commit 76aa06d3543345939820bb11c3e7080c7c25e501
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Feb 4 12:30:59 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 6cb49dc9da47e314f1552a6059ec15318158dbda
Author: David Faure <faure@kde.org>
Date:   Fri Feb 4 00:58:13 2011 +0100

    Add new interface for kfileitem-action-plugins, to solve the "const" issue
    
    and deprecated the current one. Yes, all this for a const, but that
    const makes writing plugins quite troublesome (const_cast or mutable),
    as afiestas found out.

commit ed0f2cc677d0adee7588846c0e818255aa476ea2
Author: Nick Shaforostoff <shafff@ukr.net>
Date:   Thu Feb 3 11:31:49 2011 -0500

    backport a fix for bug 207791 (guard ActionCollection with a QPointer.)

commit 8b47a880ece1a41b6685b6b12a2074e8c3a9167d
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Wed Feb 2 22:16:14 2011 -0500

    Switched back to using javscript to parse forms for login information due to problems
    with QWebElement when form inputs are nested inside of a table row.
    
    BUG: 256029
    FIXED-IN: 4.6.1

commit a92a7410c1faf5e6ea7fc764f9feb990800f4d63
Author: David Faure <faure@kde.org>
Date:   Wed Feb 2 16:31:01 2011 +0100

    Fix crash when a glob file refers to a mimetype that doesn't exist anymore.
    
    This made KMimeType::findByUrl return NULL, which is never supposed to happen.
    FIXED-IN: 4.6.1
    BUG: 265188

commit 3aed9afe198f26bd438f0206eae85b04a8120090
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Feb 2 14:02:53 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 590a190134c288621bcdd53bb168875e91b560ce
Author: Marco Martin <notmart@gmail.com>
Date:   Tue Feb 1 21:15:49 2011 +0100

    don't collapse when it's destroying
    try to not crash upon deletion of the extender

commit 8f8ebc525fc64d350fce04b34e74f0181ab66ca8
Author: Marco Martin <notmart@gmail.com>
Date:   Tue Feb 1 19:25:43 2011 +0100

    fix background when dragging
    use the dialog background frame instead of the transparent extender background

commit 62a6450bdbe65a12afa64a8ab96381431a9e4d69
Author: Andreas Pakulat <apaku@gmx.de>
Date:   Mon Jan 31 21:51:57 2011 +0100

    Make sure Soprano includes are found.
    
    This adds the soprano include directory to the includes and fixes
    compilation in case soprano is installed in a separate prefix.

commit c24113d04819b48022daece5f123015d68701a46
Author: David Faure <faure@kde.org>
Date:   Mon Jan 31 14:51:52 2011 +0100

    Harden SSL verification against poisoned DNS attacks
    
    ... in the case of certificates that are issued against an IP address rather than a hostname.
    Patch by Tomas Hoger / Red Hat Security Response Team, reviewed by Jeff Mitchell and Richard Moore.

commit b1ca51304f8ae89925b09c8d7f1e3180d2ef27b4
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 13:26:52 2011 -0800

    set status correctly

commit d9fea7c1468bcc3f49b2b088de83c11fa3677685
Author: Patrick von Reth <patrick.vonreth@gmail.com>
Date:   Fri Jan 28 20:42:47 2011 +0000

    backport of r1217817
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1217818

commit c4edf16f4655dc0a5dc96bb52a7a4534f55b95d1
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Jan 28 13:04:54 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1217760

commit bbed85ac9a2c586f6995c19bc953a2883ace8601
Author: Luboš Luňák <l.lunak@kde.org>
Date:   Wed Jan 26 16:20:53 2011 +0000

    Backport r1216758.
    Make KFileDialog automatically choose file type from typed extension
    also when file types are specified manually and not as mimetypes.
    (http://svn.reviewboard.kde.org/r/6325)
    
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1217310

commit 10c0af3f08238b2126ada62e74e87a45c6cbcf4d
Author: Luboš Luňák <l.lunak@kde.org>
Date:   Wed Jan 26 16:19:30 2011 +0000

    Backport r1216757.
    Make KMimeTypeRepository::matchFileName() public as
    KMimeType::matchFileName(), as filename pattern matching may
    be occassionally useful outside of kdelibs.
    
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1217308

commit 61c2deaddd25ea6db0cdc99f6f25c7497dc8d5b5
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Wed Jan 26 13:49:42 2011 +0000

    Backported the fix for BR#263817 from trunk (r1217274).
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1217275

commit cef4337512f5104e3a09da3ab3f9fbe6e4f61af5
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Jan 25 13:55:14 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1217038

commit 2f862ede1c2f1f342bf6174eb7b26298ee63f330
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Mon Jan 24 05:17:38 2011 +0000

    Backport the fix for BR#244215.
    
    BUG:244215
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216579

commit 54bb0526e5bd11572f36d3eadae3dd2f6a64186c
Author: Marco Martin <notmart@gmail.com>
Date:   Sat Jan 22 22:14:21 2011 +0000

    backport: realign the widget if it's resized
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216379

commit 1f5ca14228c2035e9493163dbddb947083c3110c
Author: Marco Martin <notmart@gmail.com>
Date:   Sat Jan 22 22:02:55 2011 +0000

    backport: immediately hide the focusindicator if the theme follows native style
    
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216377

commit 5af281b2da5804b1af4bdc0a32a91bc0d265d118
Author: Marco Martin <notmart@gmail.com>
Date:   Sat Jan 22 17:56:04 2011 +0000

    backport: if the theme wants to paint with the system style just let the proxy paint
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216325

commit bf506997f74be25f01e45672472f1cc1f1171c2a
Author: Marco Martin <notmart@gmail.com>
Date:   Sat Jan 22 17:50:30 2011 +0000

    backport: if the theme wants to use the system style, disable completely any focusindicator
    
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216323

commit 45467da770cf5d3b0dbb1f7199c73650293ef176
Author: Marco Martin <notmart@gmail.com>
Date:   Sat Jan 22 13:06:44 2011 +0000

    backportrevert behavioural change in framesvg, repaint glithches--
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216287

commit a6b9c35fa9a1da53420bb9b0d643892e85f08db8
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Fri Jan 21 20:43:55 2011 +0000

    When editing the name of file items opening a context-menu for the text-editor may not result in closing the rename-operation. Thanks to Zé for the patch!
    
    BUG: 182353
    FIXED-IN: 4.6.1
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216194

commit 7f8b4764577f491ffe536973c60eeb5229cc7d4e
Author: Marco Martin <notmart@gmail.com>
Date:   Fri Jan 21 13:18:38 2011 +0000

    backport fix for the second null paramenter of paintframe
    
    svn path=/branches/KDE/4.6/kdelibs/; revision=1216115
