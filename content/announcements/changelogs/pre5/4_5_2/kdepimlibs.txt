------------------------------------------------------------------------
r1168707 | vkrause | 2010-08-27 20:15:44 +1200 (Fri, 27 Aug 2010) | 12 lines

Merged revisions 1166311 via svnmerge from 
svn+ssh://vkrause@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1166311 | vkrause | 2010-08-21 16:05:14 +0200 (Sat, 21 Aug 2010) | 5 lines
  
  Revert 1162863, headers are moved from one content to another here, not
  copied, so we can't just delete them. Fixes a few unit test crashes.
  
  CCMAIL: iamsergio@gmail.com
........

------------------------------------------------------------------------
r1168838 | vkrause | 2010-08-28 03:00:49 +1200 (Sat, 28 Aug 2010) | 15 lines

Merged revisions 1168823 via svnmerge from 
svn+ssh://vkrause@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1168823 | vkrause | 2010-08-27 16:21:46 +0200 (Fri, 27 Aug 2010) | 8 lines
  
  Don't wait indefinitely for a session thread to terminate.
  
  This just covers a bug a few layers up, where for some reason the IMAP
  resource attempts a reconnect for no good reason and fails to stop the
  IDLE session which hangs in an sync read which does not terminate itself
  since the socket is still working perfectly fine. Nevertheless, it doesn't
  hurt to have a sane fallback in case something goes wrong.
........

------------------------------------------------------------------------
r1170662 | lueck | 2010-09-02 07:28:19 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1173140 | otrichet | 2010-09-09 06:24:39 +1200 (Thu, 09 Sep 2010) | 13 lines

Fix an infinite loop retrieving the password of a mail transport when TransportManager::schedule() is used and the stored password is empty.

[...]
#9719 0x00007fffef31f979 in MailTransport::TransportManager::loadPasswords (this=0x1144550) at /home/kde/dev/src/kdepimlibs/mailtransport/transportmanager.cpp:629
#9720 0x00007fffef31675a in MailTransport::Transport::password (this=0x11834c0) at /home/kde/dev/src/kdepimlibs/mailtransport/transport.cpp:82
#9721 0x00007fffef3386ef in MailTransport::SmtpJob::startSmtpJob (this=0x1187410) at /home/kde/dev/src/kdepimlibs/mailtransport/smtpjob.cpp:159
#9722 0x00007fffef337c1e in MailTransport::SmtpJob::doStart (this=0x1187410) at /home/kde/dev/src/kdepimlibs/mailtransport/smtpjob.cpp:114
#9723 0x00007fffef335344 in MailTransport::TransportJob::start (this=0x1187410) at /home/kde/dev/src/kdepimlibs/mailtransport/transportjob.cpp:129
#9724 0x00007fffef31f979 in MailTransport::TransportManager::loadPasswords (this=0x1144550) at /home/kde/dev/src/kdepimlibs/mailtransport/transportmanager.cpp:629
[...]

review: http://reviewboard.kde.org/r/2579/

------------------------------------------------------------------------
r1173187 | cgiboudeaux | 2010-09-09 10:14:00 +1200 (Thu, 09 Sep 2010) | 3 lines

Backport r1173168 from trunk to 4.5:
Work around an incompatible change in libical>0.46.

------------------------------------------------------------------------
r1174948 | jlayt | 2010-09-14 04:53:31 +1200 (Tue, 14 Sep 2010) | 2 lines

Add Croatia holiday data file

------------------------------------------------------------------------
r1175669 | jlayt | 2010-09-16 02:22:27 +1200 (Thu, 16 Sep 2010) | 7 lines

Improve efficiency of KHolidays by only parsing for required calendar systems.

Backport of r1175645

CCBUG: 251127


------------------------------------------------------------------------
r1177500 | skelly | 2010-09-21 00:03:02 +1200 (Tue, 21 Sep 2010) | 1 line

Backport 1177499
------------------------------------------------------------------------
r1179127 | mlaurent | 2010-09-25 04:29:55 +1200 (Sat, 25 Sep 2010) | 6 lines

Fix bug #215962 "Sending mail error - "Transport '....' is invalid"" 
kmail doesn't support transport with a name which has a space at begin.
Not easy to fix in kmail => remove begin space 
BUG: 215962


------------------------------------------------------------------------
r1179472 | krake | 2010-09-26 04:27:36 +1300 (Sun, 26 Sep 2010) | 3 lines

Fix prepending of custom tasks. These were prepended to the generic task queue which is processed after e.g. the change relay queue.
Fix deferTask to put the task back into the queue it came from and at its beginning in order not to change the ordering of tasks, e.g. process an item change before an item add

------------------------------------------------------------------------
r1179900 | tokoe | 2010-09-27 01:49:41 +1300 (Mon, 27 Sep 2010) | 16 lines

Merged revisions 1172043 via svnmerge from 
svn+ssh://tokoe@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1172043 | vkrause | 2010-09-06 12:07:53 +0200 (Mon, 06 Sep 2010) | 9 lines
  
  Fix notification filtering in case we are both filtering on resources
  and mimetypes, and only the mimetype matches. It worked for items but
  not for collections, since we cannot filter them on mimetypes at this
  point. Therefore, continue processing if the resource filter did not
  match but there is a mimetype filter set, to evaluate the following
  filters as well.
  
  BUG: 250056
........

------------------------------------------------------------------------
r1179906 | tokoe | 2010-09-27 02:25:17 +1300 (Mon, 27 Sep 2010) | 13 lines

Merged revisions 1179520,1179533 via svnmerge from 
svn+ssh://tokoe@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1179520 | vkrause | 2010-09-25 20:44:37 +0200 (Sat, 25 Sep 2010) | 2 lines
  
  add a few more test cases for quote()
........
  r1179533 | vkrause | 2010-09-25 21:17:05 +0200 (Sat, 25 Sep 2010) | 2 lines
  
  add unit tests for join()
........

------------------------------------------------------------------------
r1180971 | mlaurent | 2010-09-30 04:58:36 +1300 (Thu, 30 Sep 2010) | 5 lines

Backport fix for #252704 (crash when we paste a image)
and define "FIX_KMAIL_INSERT_IMAGE" to allow to compile with kde 4.4.x when
we use this new function.
This crash is very bad so I backport this fix

------------------------------------------------------------------------
r1180981 | mlaurent | 2010-09-30 05:26:28 +1300 (Thu, 30 Sep 2010) | 2 lines

Forgot to save file before to commit

------------------------------------------------------------------------
r1181385 | mueller | 2010-10-01 10:12:06 +1300 (Fri, 01 Oct 2010) | 2 lines

bump version

------------------------------------------------------------------------
