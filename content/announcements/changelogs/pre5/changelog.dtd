<!--
  Copyright (c) 2007 Bram Schoenmakers <bramschoenmakers@kde.nl>

  This file defines how to create changelog entries for the kde.org website.
-->

<!ELEMENT changelog (release*)>

<!-- Represents a full KDE release with it's official modules. -->
<!ELEMENT release (module*)>
<!ATTLIST release version CDATA #REQUIRED
                  date CDATA #REQUIRED
>

<!ELEMENT module (product*)>
<!ATTLIST module name CDATA #REQUIRED
                 homepage CDATA #IMPLIED
                 reponame CDATA #IMPLIED
>

<!-- The product is the element representing your application / library.
       name: The name of your application / library
       svnname: The name as it is reprsented in SVN. For example: the product
       "Quanta Plus" has a folder "quanta" in SVN.
       homepage: Homepage of your application / library.
-->

<!-- TODO: svnname attribute in for cases like "Quanta Plus", which does not
the "quanta" directory in websvn" -->
<!ELEMENT product (component|bugfix|feature|improvement|optimize)+>
<!ATTLIST product name CDATA #REQUIRED
                  homepage CDATA #IMPLIED
                  reponame CDATA #IMPLIED
>

<!-- Used to group bugfixes/features inside an application / library. -->
<!ELEMENT component (bugfix|feature|improvement|optimize)+>
<!ATTLIST component name CDATA #REQUIRED
                    reponame CDATA #IMPLIED
>

<!-- Describes a bugfix entry.
       rev:   SVN revision number, eventually seperated by spaces.
       bugno: Bug number at KDE Bugzilla, eventually seperated by spaces
       class: The severity of the bugfix (minor, normal, crash, severe, grave)
              NOTE: Not yet processed by the stylesheet.
-->
<!ELEMENT bugfix (#PCDATA|a|code)*>
<!ATTLIST bugfix rev CDATA #IMPLIED
                 bugno CDATA #IMPLIED
                 class (minor|normal|crash|severe|grave) #IMPLIED
>

<!-- See bugfix entry -->
<!ELEMENT feature (#PCDATA|a|code)*>
<!ATTLIST feature rev CDATA #IMPLIED
                  bugno CDATA #IMPLIED
>

<!-- See bugfix entry -->
<!ELEMENT improvement (#PCDATA|a|code)*>
<!ATTLIST improvement rev CDATA #IMPLIED
                      bugno CDATA #IMPLIED
>

<!-- See bugfix entry -->
<!ELEMENT optimize (#PCDATA|a|code)*>
<!ATTLIST optimize rev CDATA #IMPLIED
                   bugno CDATA #IMPLIED
>

<!ELEMENT a (#PCDATA)>
<!ATTLIST a href CDATA #REQUIRED
>

<!ELEMENT code (#PCDATA)>
