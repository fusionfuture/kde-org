---
aliases:
- ../changelog4_2to4_2_1
hidden: true
title: KDE 4.2.1 Changelog
---

<h2>Changes in KDE 4.2.1</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_2_1/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Repair klauncher support for unique-applications like konsole. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162729">162729</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918564&amp;view=rev">918564</a>. </li>
        <li class="bugfix ">Fix complex crash which could happen for instance when installing OSX widgets in plasma. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182138">182138</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918117&amp;view=rev">918117</a>. </li>
        <li class="bugfix ">Make sure klauncher doesn't hang when kdeinit4 dies. See SVN commit <a href="http://websvn.kde.org/?rev=918403&amp;view=rev">918403</a>. </li>
        <li class="bugfix ">Fix parsing of dates when using a two-digits year format. See SVN commit <a href="http://websvn.kde.org/?rev=928370&amp;view=rev">928370</a>. </li>
      </ul>
      </div>
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Ctrl+A, Ctrl+E and Ctrl+U in lineedits (and Ctrl+A in textedits) so they have priority over application actions (e.g. in konqueror). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=181180">181180</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=917997&amp;view=rev">917997</a>. </li>
        <li class="bugfix ">Fix handling of &lt;Tab&gt; in completion popup (especially in konqueror). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=167135">167135</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=920747&amp;view=rev">920747</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Properly update the directory view when aborting a move operation (e.g. disk full), otherwise confusion and data loss could happen. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=118593">118593</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=917170&amp;view=rev">917170</a>. </li>
        <li class="bugfix ">Fix assert failure when smb.conf contains "path=" (empty value). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184144">184144</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928071&amp;view=rev">928071</a>. </li>
        <li class="bugfix ">Show "l" rather than "d" in the permission string for symlinks. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162544">162544</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=927932&amp;view=rev">927932</a>. </li>
        <li class="bugfix ">Fix error message when starting a command with a full path. See SVN commit <a href="http://websvn.kde.org/?rev=926945&amp;view=rev">926945</a>. </li>
        <li class="bugfix ">Fix problems with accentuated paths in kioslaves (e.g. when trashing a file), with Qt-4.5-rc1. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184496">184496</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928331&amp;view=rev">928331</a>. </li>
      </ul>
      </div>
      <h4><a name="kio_http">kio_http</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Various authentication code improvements. See SVN commits <a href="http://websvn.kde.org/?rev=929627&amp;view=rev">929627</a> and <a href="http://websvn.kde.org/?rev=914548&amp;view=rev">914548</a>. </li>
        <li class="bugfix ">Handle invalid \n separation of headers and payload; fixes freezes on 
        some websites using some amazon.com services.  See SVN commit <a href="http://websvn.kde.org/?rev=929627&amp;view=rev">929627</a>. </li>
        <li class="bugfix ">Don't do an extra blocking read when the reply is ultra-small. Fixes freezes on abclinuxu.cz. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=180631">180631</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=926999&amp;view=rev">926999</a>. </li>
      </ul>
      </div>
      <h4><a name="kjs">kjs</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix debugger core crashes on syntax errors. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=175578">175578</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=926127&amp;view=rev">926127</a>. </li>
        <li class="bugfix ">Fix a crash with sparse arrays without a dense vector component. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=180605">180605</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=922902&amp;view=rev">922902</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">border-radius support extended to dashed and dotted lines. See SVN commit <a href="http://websvn.kde.org/?rev=906876&amp;view=rev">906876</a>. </li>
          <li class="improvement">Added Ctrl+Shift+U for "View Frame Source". See SVN commit <a href="http://websvn.kde.org/?rev=918042&amp;view=rev">918042</a>. </li>
      </ul>
      <em>Optimizations:</em><ul>
        <li class="optimize">Take advantage of better composition mode support in recent Qt versions to avoid roundtrips in background pre-tiling. See SVN commit <a href="http://websvn.kde.org/?rev=923396&amp;view=rev">923396</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix problems with various shortcuts in frames. See SVN commits <a href="http://websvn.kde.org/?rev=907379&amp;view=rev">907379</a>, <a href="http://websvn.kde.org/?rev=907394&amp;view=rev">907394</a> and <a href="http://websvn.kde.org/?rev=918042&amp;view=rev">918042</a>. </li>
        <li class="bugfix ">Make sure the view receives focus properly so it can be scrolled immediately in Konqueror.  See SVN commit <a href="http://websvn.kde.org/?rev=908982&amp;view=rev">908982</a>. </li>
        <li class="bugfix ">Don't paint background for &lt;input type="image"&gt;. Fixes the amazon.com button. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=179795">179795</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=909004&amp;view=rev">909004</a>. </li>
        <li class="bugfix ">Make sure DOM objects' constructor property does not point to the default Object constructor. Fixes hangs on google maps. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=176730">176730</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=183251">183251</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=922888&amp;view=rev">922888</a>. </li>
        <li class="bugfix ">Don't crash if someone tries to set the body to a non-Node. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183457">183457</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=922913&amp;view=rev">922913</a>. </li>
        <li class="bugfix ">Make sure (lazily) the stylesheets collection is up-to-date when accessed. Fixes www.webtoolkit.eu/wt. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182230">182230</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=922955&amp;view=rev">922955</a> and <a href="http://websvn.kde.org/?rev=926278&amp;view=rev">926278</a>. </li>
        <li class="bugfix ">Handle the mode parameter for insertAdjacentHTML case-insensitively; fixes some operations in yui 
        non-DOM mode; affected older reviewboard versions. See SVN commit <a href="http://websvn.kde.org/?rev=917490&amp;view=rev">917490</a>. </li>
        <li class="bugfix ">Remove workaround for emergency frame creation, it can cause crashes when restoring frames going back. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170185">170185</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=926130&amp;view=rev">926130</a>. </li>
        <li class="bugfix ">Fix contentDocument on hidden &lt;object&gt; elements. See SVN commit <a href="http://websvn.kde.org/?rev=922874&amp;view=rev">922874</a>. </li>
        <li class="bugfix ">make &lt;frameset onload=...&gt; properly should set a window event listener, not a DOM one. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183455">183455</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=924023&amp;view=rev">924023</a> and <a href="http://websvn.kde.org/?rev=926101&amp;view=rev">926101</a>. </li>
        <li class="bugfix ">Make createContextualFragment work when called on a range starting with a document; shows up in tinyMCE. See SVN commit <a href="http://websvn.kde.org/?rev=926156&amp;view=rev">926156</a>. </li>
        <li class="bugfix ">Remove excessive paranoia in file upload controls; fixes rapidshare.com uploads. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184679">184679</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=929635&amp;view=rev">929635</a>. </li>
        <li class="bugfix ">Fix encoding of surrogate pairs in form submissions. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=154142">154142</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=929608&amp;view=rev">929608</a>. </li>
        <li class="bugfix ">Fix a GC bug in TreeWalker. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172268">172268</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=926292&amp;view=rev">926292</a>. </li>
        <li class="bugfix ">Make window.sidebar writeable (and DontEnum).  Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183993">183993</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=926141&amp;view=rev">926141</a>. </li>
        <li class="bugfix ">Improve the debugger UI's handling of syntax errors. See SVN commit <a href="http://websvn.kde.org/?rev=926127&amp;view=rev">926127</a>. </li>
        <li class="bugfix ">Fix Ctrl+U and Ctrl+I being ambiguous in pages with frames. See SVN commit <a href="http://websvn.kde.org/?rev=918042&amp;view=rev">918042</a>. </li>
      </ul>
      </div>
      <h4><a name="kded/kbuildsycoca4">kded/kbuildsycoca4</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix regression: the directories with desktop files were not watched anymore, so users had to run kbuildsycoca4 by hand after installing new desktop files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182472">182472</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918838&amp;view=rev">918838</a>. </li>
        <li class="bugfix ">Fix duplicated applications in the K menu and in keditfiletype, because kded was creating kservices with absolute paths and not letting local ones override global ones. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=179946">179946</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=179462">179462</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=182060">182060</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=184716">184716</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=921138&amp;view=rev">921138</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_2_1/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixed regression that a double click is required for the Folders Panel to show the content. See SVN commit <a href="http://websvn.kde.org/?rev=919653&amp;view=rev">919653</a>. </li>
        <li class="bugfix ">Display correct folder names in tabs, if they contain a '&amp;' character. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=181765">181765</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=919023&amp;view=rev">919023</a>. </li>
        <li class="bugfix ">Fixed issue that sometimes the 'Create New' menu was disabled although creating files was allowed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183812">183812</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=924120&amp;view=rev">924120</a>. </li>
        <li class="bugfix ">Hide the tooltip when a context menu is opened in the column view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184053">184053</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=924891&amp;view=rev">924891</a>. </li>
        <li class="bugfix ">Don't show a rubberband selection when trying to move the selection toggle. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184178">184178</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=927488&amp;view=rev">927488</a>. </li>
        <li class="bugfix ">Fixed issue that invoking Dolphin with a specified path is ignored when another Dolphin window is already open. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169016">169016</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=927549&amp;view=rev">927549</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix drag-n-drop regression: couldn't drop URLs onto the HTML part anymore. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=177925">177925</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=930537&amp;view=rev">930537</a>. </li>
      </ul>
      </div>
      <h4><a name="plasma">plasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixes for running Python applets similtaneously with Python data engines. See SVN commit <a href="http://websvn.kde.org/?rev=917410&amp;view=rev">917410</a>. </li>
      </ul>
      </div>
      <h4><a name="kio_trash">kio_trash</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">fix crash. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=177866">177866</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=924870&amp;view=rev">924870</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="modulehomepage"> [ <a href="http://edu.kde.org/">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_2_1/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/kgeography/" name="kgeography">kgeography</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix bug in South Africa map. See SVN commit <a href="http://websvn.kde.org/?rev=924430&amp;view=rev">924430</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="modulehomepage"> [ <a href="http://games.kde.org/">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_2_1/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://games.kde.org/game.php?game=kshisen" name="kshisen">kshisen</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix bug where game is playable in pause mode. See SVN commit <a href="http://websvn.kde.org/?rev=917457&amp;view=rev">917457</a>. </li>
        <li class="bugfix ">Fix bug where cheat mode is not reset on game restart. See SVN commit <a href="http://websvn.kde.org/?rev=919948&amp;view=rev">919948</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_2_1/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Add a 'Find Previous' entry. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184230">184230</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=926107&amp;view=rev">926107</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">DjVu backend: fix hanging on malformed document loading. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182971">182971</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=920672&amp;view=rev">920672</a>. </li>
        <li class="bugfix crash">CHM backend: avoid crashing during loading because of user events. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=178539">178539</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=920833&amp;view=rev">920833</a>. </li>
        <li class="bugfix ">CHM backend: do not hang when a HTML page does not load correctly. See SVN commit <a href="http://websvn.kde.org/?rev=922591&amp;view=rev">922591</a>. </li>
        <li class="bugfix crash">CHM backend: do not crash on TOC entries with no associated URLs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183363">183363</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=922604&amp;view=rev">922604</a>. </li>
        <li class="bugfix ">Do not scroll the thumbnail list when resizing the navigation panel. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183475">183475</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=922824&amp;view=rev">922824</a>. </li>
        <li class="bugfix crash">(Windows only) Do not crash because of miscalculated system memory. See SVN commit <a href="http://websvn.kde.org/?rev=923029&amp;view=rev">923029</a>. </li>
        <li class="bugfix ">Fix start from current screen for presentation mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=185013">185013</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928890&amp;view=rev">928890</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_2_1/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kontact.kde.org/kmail/" name="kmail">KMail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't ask for the wallet password on exit. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=163413">163413</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918208&amp;view=rev">918208</a>. </li>
        <li class="bugfix ">Make mailto links with body and subject work again. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=108974">108974</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=117293">117293</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918210&amp;view=rev">918210</a>. </li>
        <li class="bugfix ">Fix incorrect attachment file encoding if the filename contains a percent sign. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=173503">173503</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918212&amp;view=rev">918212</a>. </li>
        <li class="bugfix ">Fix the completion order for LDAP not being remembered. See SVN commit <a href="http://websvn.kde.org/?rev=918214&amp;view=rev">918214</a>. </li>
        <li class="bugfix ">Improve display of SMIME attachments that don't follow the RFC and fail to specify the protocol in the header. See SVN commit <a href="http://websvn.kde.org/?rev=925418&amp;view=rev">925418</a>. </li>
        <li class="bugfix ">Use better icon for the "ignore thread" action. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183569">183569</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=925422&amp;view=rev">925422</a>. </li>
        <li class="bugfix ">Don't add POP3 and local accounts to the list in the manage sieve scripts dialog. See SVN commit <a href="http://websvn.kde.org/?rev=925423&amp;view=rev">925423</a>. </li>
        <li class="bugfix ">Make sure to not execute executables when clicking a URL in the message window. See SVN commit <a href="http://websvn.kde.org/?rev=927081&amp;view=rev">927081</a>. </li>
        <li class="bugfix ">When copying from the message window, remove the non-breaking space characters. See SVN commit <a href="http://websvn.kde.org/?rev=928212&amp;view=rev">928212</a>. </li>
        <li class="bugfix ">In the folder selection dialog, allow creating subfolders of the local folder root. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=128796">128796</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928215&amp;view=rev">928215</a>. </li>
        <li class="bugfix ">Use the correct folder icons in the folder selection dialog. See SVN commit <a href="http://websvn.kde.org/?rev=928216&amp;view=rev">928216</a>. </li>
        <li class="bugfix ">Fix the signature certificate text for SMIME signatures. See SVN commits <a href="http://websvn.kde.org/?rev=928270&amp;view=rev">928270</a> and <a href="http://websvn.kde.org/?rev=925419&amp;view=rev">925419</a>. </li>
        <li class="bugfix ">Fix crash when deleting mails. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174839">174839</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=930647&amp;view=rev">930647</a>. </li>
        <li class="bugfix ">Fix the layout of the add snippet dialog. See SVN commit <a href="http://websvn.kde.org/?rev=931308&amp;view=rev">931308</a>. </li>
        <li class="bugfix ">Display "No Subject" again in the message list, if the subject is empty. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184854">184854</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=931310&amp;view=rev">931310</a>. </li>
        <li class="bugfix ">Fix mail address selection in the redirect dialog. See SVN commit <a href="http://websvn.kde.org/?rev=918215&amp;view=rev">918215</a>. </li>
      </ul>
      </div>
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make command alarms work in konsole windows when a KDE3 command line is stored in KAlarm's configuration file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=175623">175623</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918286&amp;view=rev">918286</a>. </li>
      </ul>
      </div>
    </div>
    
    <h3 id="kdebindings"><a name="kdebindings">kdebindings</a><span class="allsvnchanges"> [ <a href="4_2_1/kdebindings.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://techbase.kde.org/Development/Languages/Python" name="pykde4">PyKDE4</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Added some CPython GIL control to kpythonpluginfactory to stop crashes in situations where one Python plugin indirectly calls another. See SVN commit <a href="http://websvn.kde.org/?rev=917422&amp;view=rev">917422</a>. </li>
        <li class="bugfix ">Updated and fixed the generated API docs. Some namespaces in kdecore were missing. See SVN commit <a href="http://websvn.kde.org/?rev=926471&amp;view=rev">926471</a>. </li>
        <li class="bugfix ">Fixed a heap of memory and object ownership related bugs in the Plasma bindings. See SVN commit <a href="http://websvn.kde.org/?rev=917978&amp;view=rev">917978</a>. </li>
        <li class="bugfix ">Added method FlashingLabel.flash(QString,int,QTextOption) to the Plasma bindings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184075">184075</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928052&amp;view=rev">928052</a>. </li>
      </ul>
      </div>
    </div>

    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_2_1/kdemultimedia.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Always use the right delete method for PlaylistItems.
          This should fix at least one cause of one of the most frequently duplicated crash bugs in JuK. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169125">169125</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=926737&amp;view=rev">926737</a>. </li>
        <li class="bugfix crash">Don't crash on JuK shutdown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=175035">175035</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=918041&amp;view=rev">918041</a>. </li>
        <li class="bugfix ">Correctly handle Phonon states to avoid a race
          condition causing premature playback stopping. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=176329">176329</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=915880&amp;view=rev">915880</a>. </li>
        <li class="bugfix ">Keep the history playlist correctly sorted after removing a
          track that had been in history from disk. See SVN commit <a href="http://websvn.kde.org/?rev=910699&amp;view=rev">910699</a>. </li>
        <li class="bugfix crash">Don't crash if a music track in the history is
          no longer on disk. See SVN commit <a href="http://websvn.kde.org/?rev=910695&amp;view=rev">910695</a>. </li>
        <li class="bugfix ">Fix a stray "question mark" icon. See SVN commit <a href="http://websvn.kde.org/?rev=909551&amp;view=rev">909551</a>. </li>
        <li class="bugfix crash">Fix a crash if the track announcement popup
          were to be deleted while the system tray was animating it. See SVN commit <a href="http://websvn.kde.org/?rev=909743&amp;view=rev">909743</a>. </li>
      </ul>
      </div>
    </div>

    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_2_1/kdenetwork.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kopete.kde.org" name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix the indent contacts setting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=165252">165252</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928782&amp;view=rev">928782</a>. </li>
        <li class="bugfix ">Fix bug where the invisible flag does not persist after a status change. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=171733">171733</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=913702&amp;view=rev">913702</a>. </li>
        <li class="bugfix ">Fix display name not retrieved/saved in the WLM protocol. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182366">182366</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928776&amp;view=rev">928776</a>. </li>
        <li class="bugfix ">Fix a big memory leak in the WLM protocol. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183930">183930</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=929115&amp;view=rev">929115</a>. </li>
        <li class="bugfix ">Fix downloading buddy icons in Yahoo protocol. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146118">146118</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=920984&amp;view=rev">920984</a>. </li>
        <li class="bugfix ">Fix a bug in Yahoo protocol where contacts where shown offline altough they were not. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=150482">150482</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=921978&amp;view=rev">921978</a>. </li>
        <li class="bugfix ">Fix a bug where Yahoo disconnects immediately when connecting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=163307">163307</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=923817&amp;view=rev">923817</a>. </li>
        <li class="bugfix crash">Fix a crash on Yahoo disconnect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=166553">166553</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=908162&amp;view=rev">908162</a>. </li>
      </ul>
      </div>
    </div>

    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="4_2_1/kdesdk.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kompare">Kompare</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Do not try to show character changes if more than half of the line changed, as it will just match single characters arbitrarily. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=75794">75794</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix detection of inline differences not working at the start of the line. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=89781">89781</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Fix not exiting properly and getting restored in the next session. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=102800">102800</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Fix not exiting properly when File / Compare Files... used. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=103651">103651</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Fix infinite recursion when not having a common root folder in a multi-file diff. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=107489">107489</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Display correct line numbers after applying a change. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145956">145956</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix crash">Fix crash when saving. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=165421">165421</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix crash">Fix Unapply Difference being enabled even when impossible and crashing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169692">169692</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix crash">Fix crash when closing the application. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174924">174924</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Fix unreadable text with dark color schemes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=175251">175251</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix crash">Fix crash when clicking Apply in the font dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=176797">176797</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Fix both the W32 and KDE file dialogs being shown one after the other. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=176804">176804</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix crash">Fix crash with Qt 4.5 during the invocation of diff. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182792">182792</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Fix Previous Difference not working on the first difference of a file (now switches to the last difference in the previous file as expected). See SVN commit <a href="http://websvn.kde.org/?rev=928150&amp;view=rev">928150</a>. </li>
        <li class="bugfix ">Fix help button in the Preferences dialog not referring to the correct help page. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=69081">69081</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=930695&amp;view=rev">930695</a>. </li>
        <li class="bugfix ">Fix saving a diff to a remote ioslave. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=76904">76904</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=930700&amp;view=rev">930700</a>. </li>
        <li class="bugfix ">Fix saving files with applied changes to a remote ioslave. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=81088">81088</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=930700&amp;view=rev">930700</a>. </li>
        <li class="bugfix ">Fix new directory being created instead of new file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126870">126870</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=930700&amp;view=rev">930700</a>. </li>
        <li class="bugfix ">Fix wrong destination filename being displayed when reading unified diffs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=139212">139212</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=930701&amp;view=rev">930701</a>. </li>
      </ul>
      </div>
    </div>

    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_2_1/kdeutils.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Remove GnuPG status message if encrypted text does not ent with \n. See SVN commit <a href="http://websvn.kde.org/?rev=924884&amp;view=rev">924884</a>. </li>
      </ul>
      </div>
    </div>