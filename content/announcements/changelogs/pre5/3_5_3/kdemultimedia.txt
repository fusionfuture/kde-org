2006-03-30 00:55 +0000 [r524224]  dgp

	* branches/KDE/3.5/kdemultimedia/configure.in.in,
	  branches/KDE/3.5/kdemultimedia/juk/configure.in.in: Move the arts
	  check to top level configure.in.in as it's used by other
	  subdirectories other than juk.

2006-04-07 15:32 +0000 [r527293]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/tagrenameroptions.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/tagrenameroptions.h: Fix bug
	  124976 (JuK trying to add unknown category in File Renamer) by
	  comparing translated strings with translated strings. Thanks to
	  the bug reporter for the patch. BUG:124976

2006-04-12 12:40 +0000 [r529030]  coolo

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp: nice
	  guess. The chances were just 50:50 to get it right ;) The
	  difference is 0 for IDE, but noticably for SATA

2006-04-20 08:14 +0000 [r531797]  seb

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.h: * When
	  no tracks are selected, allow the user to rip entire album,
	  instead of showing a "no tracks selected..." box * Some style
	  fixes, hope you don't mind icefox!

2006-04-20 14:09 +0000 [r531862]  seb

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.h,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/tracks.ui,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.h: Some
	  refactoring work: * QListView -> KListView * CD Tracks subclass
	  of klistviewitem I've broken item renaming, fix coming soon!

2006-04-20 14:14 +0000 [r531864]  seb

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp: Don't
	  show prepending " - " for no disc text

2006-04-20 14:22 +0000 [r531867]  seb

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp: *
	  Ensure toolbar buttons are disabled if there is no cd on app load
	  * Don't pretend to be able to edit album information if there is
	  no cd loaded

2006-04-20 14:30 +0000 [r531869]  seb

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp: Don't
	  pretend to be able to do things we can't: * Disable rip/select
	  buttons/menu entries where relevant tabs -> spaces

2006-05-01 10:10 +0000 [r536081]  seb

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp: Fix
	  regression causing renamed tracks to be disregarded when
	  ripping/encoding

2006-05-08 13:04 +0000 [r538607]  kernalex

	* branches/KDE/3.5/kdemultimedia/kscd/libwm/configure.in.in,
	  branches/KDE/3.5/kdemultimedia/kscd/libwm/plat_linux_cdda.c: even
	  wondering, why nobody complains about CDDA problems. Build with
	  CDDA was omitted for months, as "configure.in.in" leaks the same
	  workaround around <linux/cdrom.h> as all source files have.

2006-05-16 23:27 +0000 [r541685]  howells

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/kcmaudiocd.cpp:
	  Changing the slider should enable the apply/reset buttons
	  BUG:119962

2006-05-17 11:53 +0000 [r541817]  lunakl

	* branches/KDE/3.5/kdemultimedia/configure.in.in: Fix vorbis check,
	  vorbis_bitrate_addblock() in in libvorbis and not libvorbisenc -
	  the check fails with -Wl,--as-needed.

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

