KDE Project Security Advisory
=============================

Title:          ktnef: Directory Traversal
Risk Rating:    Medium
CVE:            TBC
Versions:       ktnef <= 5.4.2 (KDE Applications 16.12.2)
Date:           27 February 2017


Overview
========
A directory traversal issue was found in ktnef which can
be exploited by tricking a user into opening a malicious winmail.dat file.
The issue allows to write files with the permission of the user opening
the winmail.dat file during extraction.


Solution
========
Update to ktnef >= 5.4.3 (KDE Applications 16.12.3) (when released)

Or apply the following patch:
https://commits.kde.org/ktnef/4ff38aa15487d69021aacad4b078500f77fb4ae8

Credits
=======
Thanks to X41 D-Sec GmbH for finding the issue and providing us with
files to reproduce it
