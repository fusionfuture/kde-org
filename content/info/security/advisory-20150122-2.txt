KDE Project Security Advisory
=============================

Title:          kde-workspace, plasma-workspace: X11 clients can eavesdrop input events while screen is locked
Risk Rating:    Low
CVE:            CVE-2015-1308
Platforms:      X11
Versions:       kde-workspace >= 4.2.0, plasma-workspace < 5.1.95
Author:         Martin Gräßlin mgraesslin@kde.org
Date:           22 January 2015

Overview
========

Plasma ScreenLocker deamon (ksld) as part of ksmserver grabs keyboard and
mouse to ensure that no other X11 client is able to read the input while
the screen is locked. All input events are sent from ksld to the greeter
process showing the unlocking UI.

The vulnerability allows any X11 client (either locally or remote) to gain
access to all input events entered while the screen is locked.

Impact
======

Any application having access to the X server is able to sniff the
user's  password. An application connected to the X server might be run
by a different user or even be a remote application.

Workaround
==========

To reduce the risk it's recommended to not allow X11 clients from other
user accounts on the local system or remote X11 clients to connect to
the X server.

On kde-workspace using the "Screen locker type" "Screen saver" instead of
the default "Simple locker" can circumvent the problem.

In general disabling the screen locker also circumvents the problem.

Solution
========

For plasma-workspace upgrade to Plasma 5.1.95 or apply the following patch:
 http://commits.kde.org/plasma-workspace/0ac34dca5d6a6ea8fc5c06e1dae96fb1ad4ce7c9

Credits
=======

Thanks to Martin Gräßlin for finding and fixing the issue