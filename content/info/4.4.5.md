---
title: "KDE 4.4.5 Info Page"
---

<p>
<a href="../announcements/announce-4.4.5.php">KDE 4.4.5 was released</a> on June 30th, 2010.
</p>

<p>This page will be updated to reflect any changes in the status of the 4.4
release cycle, so check back for new information.</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
<?php include "security/advisory-20110411-1.inc" ?>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.

<h2>Download and Installation</h2>

<p>
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">Source code
 build instructions</a> are available on Techbase, our technical wiki.
</p>

<h3><a name='desktop'>KDE SC 4.4.5 Desktop release</a></h3>

<p>
  The complete source code for KDE SC 4.4.5 is available for download:
</p>

<?php
include "source-4.4.5.inc"
?>

<h3><a name="binary">Binary packages</a></h3>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE SC 4.4 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/4.4.5/">http or FTP mirrors</a>.
</p>

<p>
  Currently pre-compiled packages are available for:
</p>

<?php
include "binary-4.4.5.inc"
?>

<h2>Developer Info</h2>

<p>
If you need help porting your application to the KDE 4 Platform see the <a
href="http://websvn.kde.org/*checkout*/trunk/KDE/kdelibs/KDE4PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Developers mailing list</a>
to ask specific questions about porting your applications.
</p>

<?php
?>
